#ifndef LOGINFO_H
#define LOGINFO_H

#include <QDialog>
#include "Logger.h"

namespace Ui {
class LogInfo;
}

class LogInfo : public QDialog
{
    Q_OBJECT

public:
    explicit LogInfo(QWidget *parent = 0);
    ~LogInfo();
protected:
    bool eventFilter(QObject *o, QEvent *e);

private:
    Ui::LogInfo *ui;

    QPoint m_movePos;
    bool m_pressed;

private slots:
    void OnNewLogLine(Logger::enum_type type, QString string);
};

#endif // LOGINFO_H

#include "aboutdialog.h"
#include "ui_aboutdialog.h"
#include <QPushButton>
#include "Logger.h"
aboutDialog::aboutDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::aboutDialog)
{
    ui->setupUi(this);
    setWindowFlags(Qt::FramelessWindowHint);
    connect(ui->closeButton,&QPushButton::clicked,this,&QWidget::close);

    ui->label->installEventFilter(this);
    m_pressed = false;
}

aboutDialog::~aboutDialog()
{
    delete ui;
}

bool aboutDialog::eventFilter(QObject *o, QEvent *e)
{
    if(o == ui->label)
    {
        QMouseEvent *mEvent = (QMouseEvent *)e;
        if(e->type() == QEvent::MouseButtonPress)
        {
            m_pressed = true;
            m_movePos = mEvent->globalPos();
        }
        else if((e->type() == QEvent::MouseMove) && m_pressed)
        {
            QPoint offset = mEvent->globalPos() - m_movePos;
            move(this->x() + offset.x(), this->y() + offset.y());
            m_movePos = mEvent->globalPos();
        }
    }
}

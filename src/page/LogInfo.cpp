#include "LogInfo.h"
#include "ui_LogInfo.h"
#include "Logger.h"

LogInfo::LogInfo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LogInfo)
{
    ui->setupUi(this);
    setWindowFlags(Qt::FramelessWindowHint);
    connect(ui->closeButton,&QPushButton::clicked,this,&QWidget::close);
    connect(ui->pushButton,&QPushButton::clicked,this,&QWidget::close);
    Logger::LogInfo("log");
    //日志功能集成
    connect(Logger::GetInstance(),SIGNAL(NewLine(Logger::enum_type,QString)),this,SLOT(OnNewLogLine(Logger::enum_type,QString)),Qt::QueuedConnection);

    ui->label->installEventFilter(this);
    m_pressed = false;
}

LogInfo::~LogInfo()
{
    delete ui;
}

bool LogInfo::eventFilter(QObject *o, QEvent *e)
{
    if(o == ui->label)
    {
        QMouseEvent *mEvent = (QMouseEvent *)e;
        if(e->type() == QEvent::MouseButtonPress)
        {
            m_pressed = true;
            m_movePos = mEvent->globalPos();
        }
        else if((e->type() == QEvent::MouseMove) && m_pressed)
        {
            QPoint offset = mEvent->globalPos() - m_movePos;
            move(this->x() + offset.x(), this->y() + offset.y());
            m_movePos = mEvent->globalPos();
        }
    }
}


void LogInfo::OnNewLogLine(Logger::enum_type type, QString string) {
    //Logger::LogInfo("OnNewLogLine");
//Logger::LogInfo(string);
//    if(m_page_started && type == Logger::TYPE_ERROR && !m_error_occurred) {
//        m_error_occurred = true;
//        UpdateSysTray();
//    }
    // add line to log
    QTextCursor cursor = ui->textEdit->textCursor();
    QTextCharFormat format;
    bool should_scroll = (ui->textEdit->verticalScrollBar()->value() >= ui->textEdit->verticalScrollBar()->maximum());
    switch(type) {
        case Logger::TYPE_INFO:     format.setForeground(ui->textEdit->palette().windowText());  break;
        case Logger::TYPE_WARNING:  format.setForeground(Qt::darkYellow);                          break;
        case Logger::TYPE_ERROR:    format.setForeground(Qt::red);                                 break;
        case Logger::TYPE_STDERR:   format.setForeground(Qt::gray);                                break;
    }
    cursor.movePosition(QTextCursor::End);
    if(cursor.position() != 0)
        cursor.insertBlock();
    cursor.insertText(string, format);
    if(should_scroll)
        ui->textEdit->verticalScrollBar()->setValue(ui->textEdit->verticalScrollBar()->maximum());

}

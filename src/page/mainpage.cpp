#include "mainpage.h"
#include "ui_mainpage.h"
#include <iostream>
#include <QFileDialog>
#include <QMessageBox>
#include <unistd.h>
#include <stdio.h>
#include "Logger.h"
#include "CommandLineOptions.h"
#include "Icons.h"
#include "Dialogs.h"
#include "EnumStrings.h"
#include "NVidia.h"
#include "PageInput.h"
#include "PageOutput.h"
#include "PageRecord.h"
#include "PageDone.h"
#include <QPushButton>
#include <iostream>
#include <qprocess.h>
using namespace std;


ENUMSTRINGS(MainPage::enum_nvidia_disable_flipping) = {
    {MainPage::NVIDIA_DISABLE_FLIPPING_ASK, "ask"},
    {MainPage::NVIDIA_DISABLE_FLIPPING_YES, "yes"},
    {MainPage::NVIDIA_DISABLE_FLIPPING_NO, "no"},
};
const QString MainPage::WINDOW_CAPTION = "方德录屏";

QString res_icon_btn_start = ":/btn_mainpage_start_normal.png";
QString res_icon_btn_start_hover = ":/btn_mainpage_start_hover.png";
QString res_icon_btn_pause = ":/btn_mainpage_pause_normal.png";
QString res_icon_btn_pause_hover = ":/btn_mainpage_pause_hover.png";
QString res_icon_btn_stop = ":/btn_mainpage_stop_normal.png";
QString res_icon_btn_stop_hover = ":/btn_mainpage_stop_hover.png";
QString res_icon_btn_resume = ":/btn_mainpage_continue_normal.png";
QString res_icon_btn_resume_hover = ":/btn_mainpage_continue_hover.png";

MainPage::MainPage(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainPage)
{
    ui->setupUi(this);
    ui->filePathLineEdit->setReadOnly(true);
    setWindowFlags(Qt::FramelessWindowHint);
    setWindowTitle(WINDOW_CAPTION);
    setWindowIcon(g_icon_logo);
//    setWindowFlags(Qt::X11BypassWindowManagerHint);
//    setAttribute(Qt::WA_X11NetWmWindowTypeDock);
//    setAttribute(Qt::WA_X11NetWmWindowTypePopupMenu);

    ui->titleFrame->installEventFilter(this);
    init_old_logic();
    init_footer();

//    TitleBar();
//    this->setWindowFlags(this->windowFlags() | Qt::FramelessWindowHint);
//    this->setWindowFlags(Qt::Window|Qt::FramelessWindowHint|Qt::WindowSystemMenuHint|Qt::WindowMinimizeButtonHint|Qt::WindowMaximizeButtonHint);
//
  //  pr = new PageRecord(MainPage);

    recMenu = new QMenu(this);
    quaMenu = new QMenu(this);
    setMenu = new QMenu(this);
    screensMenu = new QMenu(this);
    audiMenu = new QMenu(this);
    typeMenu = new QMenu(this);
    srcAudioMenu = new QMenu(this);
    srcAudioGroup = new QActionGroup(srcAudioMenu);
    screenGroup = new QActionGroup(screensMenu);
    audiGroup = new QActionGroup(audiMenu);
    srcAudioGroup->setExclusive(true);
    audiGroup->setExclusive(true);
    quaGroup = new QActionGroup(quaMenu);
    quaGroup->setExclusive(true);
    noAudiAction = new QAction(audiGroup);
    sysmicAction = new QAction(audiMenu);
    sysAudiAction = new QAction(audiMenu);

//    alsaAudiAction =new QAction(audiMenu);
//    pulseAudiAction = new QAction(audiMenu);
//    jackAudiAction = new QAction(audiMenu);
    fullAction = new QAction(recMenu);
    sizeAction = new QAction(recMenu);
    openGlAction = new QAction(recMenu);
    setAction = new QAction(setMenu);
    logAction = new QAction(setMenu);
    aboutAction = new QAction(setMenu);
    highAction = new QAction(quaMenu);
    midAction = new QAction(quaMenu);
    lowAction = new QAction(quaMenu);
    sepAction = new QAction(recMenu);
    mkvAction = new QAction(typeMenu);
    mp4Action = new QAction(typeMenu);
    webmAction = new QAction(typeMenu);
    oggAction = new QAction(typeMenu);
    hintAciton = new QAction(typeMenu);
    highAction = new QAction(quaGroup);
    midAction  = new QAction(quaGroup);
    lowAction = new QAction(quaGroup);
    srcAudioAction = new QAction(audiMenu);
//    syncAudioAction = new QAction(audiGroup);

    identifyScreensFunc();
    srcAudioMenu_func();


    noAudiAction->setText("不录音");
//    syncAudioAction->setText("同时录制");
    sysmicAction->setText("麦克风声音");
    sysAudiAction->setText("系统声音");
    fullAction->setMenu(screensMenu);
    srcAudioAction->setMenu(srcAudioMenu);
    highAction->setText("高画质");
    midAction->setText("中等画质");
    lowAction->setText("低画质");


    fullAction->setText("全屏录制");
    fullAction->setCheckable(true);
    setAction->setText("设置");
    logAction->setText("日志");
    aboutAction->setText("关于");
    sizeAction->setText("区域录制");
    mkvAction->setText("mkv");
    mp4Action->setText("mp4");
    webmAction->setText("webm");
    oggAction->setText("ogg");
    hintAciton->setText("灰色部分为音视频受限导致");
    sizeAction->setCheckable(true);
    openGlAction->setText("RecordOpenGL");
    srcAudioAction->setText("声音源");
    openGlAction->setCheckable(true);
    sepAction->setSeparator(true);
    noAudiAction->setCheckable(true);
//    syncAudioAction->setCheckable(true);

    sysmicAction->setCheckable(true);
    sysAudiAction->setCheckable(true);
    srcAudioAction->setCheckable(true);

    highAction->setCheckable(true);
    midAction->setCheckable(true);
    lowAction->setCheckable(true);
    recMenu->addAction(fullAction);
    recMenu->addAction(sizeAction);
    recMenu->addAction(sepAction);
    // recMenu->addAction(openGlAction);
    audiMenu->addActions({noAudiAction,srcAudioAction});
    setMenu->addActions({setAction, logAction, aboutAction});
    typeMenu->addActions({mkvAction, mp4Action, webmAction, oggAction, hintAciton});
    quaGroup->addAction(highAction);
    quaGroup->addAction(midAction);
    quaGroup->addAction(lowAction);
    audiGroup->addAction(noAudiAction);
//    audiGroup->addAction(syncAudioAction);
    quaMenu->addActions({highAction,midAction,lowAction});
    highAction->setText(QObject::tr("高画质"));
    midAction->setText("中等画质");
    lowAction->setText("低画质");

//    connect(fullAction, &QAction::triggered, this, &MainPage::fullRecord_func);
    connect(sizeAction, &QAction::triggered, this, &MainPage::fixSizeRecord_func);
    connect(mp4Action, &QAction::triggered, this, &MainPage::mp4TypeFunc);
    connect(mkvAction, &QAction::triggered, this ,&MainPage::mkvTypeFunc);
    connect(webmAction, &QAction::triggered, this ,&MainPage::webmTypeFunc);
    connect(oggAction, &QAction::triggered, this ,&MainPage::oggTypeFunc);
    connect(noAudiAction, &QAction::triggered, this, &MainPage::noAudioFunc);
    connect(highAction, &QAction::triggered, this, &MainPage::highQuaFunc);
    connect(midAction, &QAction::triggered, this, &MainPage::midQuaFunc);
    connect(lowAction, &QAction::triggered, this, &MainPage::lowQuaFunc);
//    connect(syncAudioAction, &QAction::triggered, this, &MainPage::syncAudioFunc);



    ui->recordMenu->setStyleSheet("QPushButton::menu-indicator{image:none;}");
    ui->qualityMenu->setStyleSheet("QPushButton::menu-indicator{image:none;}");
    ui->setListBtn->setStyleSheet("QPushButton::menu-indicator{image: none;}");
    ui->formatMenu->setStyleSheet("QPushButton::menu-indicator{image:none;}");
    ui->volumeMenu->setStyleSheet("QPushButton::menu-indicator{image:none;}");
//    QString sourceAudio = PageInput::GetALSASourceName();
//    std::cout<<sourceAudio<<std::endl;

    ui->volumeMenu->setMenu(audiMenu);
    ui->recordMenu->setMenu(recMenu);
    ui->qualityMenu->setMenu(quaMenu);
    ui->setListBtn->setMenu(setMenu);
    ui->formatMenu->setMenu(typeMenu);
/*************************defalt********************/
    QString name = qgetenv("USER");
    QDir dir("/home/"+name+"/视频/nfs-screenrecorder/");
    if(!dir.exists())
    {
        dir.mkpath(".");
    }
    QString path = "/home/"+name+"/视频/nfs-screenrecorder/nfs-screenrecorder.mp4";

    ui->hSpinbox->setMaximum(9999);
    ui->wSpinBox->setMaximum(9999);

    m_page_input->m_spinbox_video_frame_rate->setValue(899);
    m_page_output->m_lineedit_file->setText(path);
    m_page_output->OnUpdateSuffixAndContainerFields();
    ui->filePathLineEdit->setText(m_page_output->m_lineedit_file->text());

    m_page_output->m_slider_h264_crf->setValue(20);
    m_page_output->m_combobox_h264_preset->setCurrentIndex(0);
    m_page_output->m_combobox_vp8_cpu_used->setCurrentIndex(2);
    m_page_output->SetVideoKBitRate(5000);

    m_page_output->m_slider_h264_crf->setValue(30);
    m_page_output->m_combobox_h264_preset->setCurrentIndex(0);
    m_page_output->m_combobox_vp8_cpu_used->setCurrentIndex(2);
    m_page_output->SetVideoKBitRate(4000);
    ui->quaDynamicLabel->setText("中等画质");


    m_page_output->m_combobox_container->setCurrentIndex(1);
    m_page_output->OnUpdateSuffixAndContainerFields();
    m_page_output->m_combobox_video_codec->setCurrentIndex(0);
    m_page_output->m_combobox_audio_codec->setCurrentIndex(0);
    ui->maxBtn->hide();
    
    ui->typeDynamicLabel->setText("MP4");
    ui->filePathLineEdit->setText(m_page_output->m_lineedit_file->text());

//    m_page_input->m_checkbox_audio_enable->setChecked(false);

    sysmicAction->setDisabled(true);
    sysAudiAction->setDisabled(true);
    noAudioFunc();

    ui->VolDynamicLabel->setText("不录音");

    ui->micCheckbox->setChecked(false);
    ui->micCheckbox->setCheckable(false);

    noAudiAction->setChecked(true);

//    int menu_x_pos = ui->recordMenu->menu()->pos().x();
//    int menu_width = ui->recordMenu->menu()->size().width();
//    int button_width = ui->recordMenu->size().width();
//    QPoint pos = QPoint(menu_x_pos - menu_width + button_width, ui->recordMenu->menu()->pos().y());
//    ui->recordMenu->menu()->move(pos);


    ptrsettingmenu = new SettingMenu(this);
    connect(setAction,&QAction::triggered,this,[=](){
    MainPage::setting_click();
    });

    connect(logAction,&QAction::triggered,this,[=](){
        MainPage::log_click();
    });
    connect(aboutAction,&QAction::triggered,this,[=](){
        MainPage::about_click();
    });


    //ptrsettingmenu->hide();
    LoadSettings();

    ptrlog = new LogInfo();

    m_page_input->SetVideoRecordCursor(true);

    int videonumber = ptrsettingmenu->GetVideoCodec();
    int audionumber = ptrsettingmenu->GetAudioCodec();
    cout<<"videonumber\t"<<videonumber<<endl;
    cout<<"audionumber\t"<<audionumber<<endl;
    Init_SetFormatEnabel(videonumber,audionumber);


    connect(&ptrsettingmenu->m_hotkey_start_pause,SIGNAL(Triggered()),this,SLOT(on_startBtn_clicked()),Qt::QueuedConnection);

    ui->attention->setVisible(false);
//    recMenu->move(ui->recordMenu->pos() + QPoint(0, 200));

    /////////////////////////////////////////////////////
    int x = ui->recordMenu->width();
    int y = ui->recordMenu->height()/2;
    QPoint pos(ui->recordMenu->mapToGlobal(QPoint(x,y)));
    recMenu->popup(pos);

    menuSlot();

}

MainPage::~MainPage()
{
    delete ui;
}

void MainPage::menuSlot()
{
    QPoint pos = ui->recordMenu->pos();
    pos.setX(pos.x() + 100);
    pos.setY(pos.y() + ui->recordMenu->height() -1);
    recMenu->popup(pos);
}

void MainPage::init_old_logic() {
//	m_page_welcome = new PageWelcome(this);
    m_page_input = new PageInput(this);
    m_page_output = new PageOutput(this);
    m_page_record = new PageRecord(this);
    m_page_done = new PageDone(this);

    m_page_input->hide();
    m_page_output->hide();
//    m_page_record->hide();
    m_page_done->hide();
}

void MainPage::init_footer() {
    m_isRecording = false;
    m_isStarted = false;
    set_widget_icon(ui->overBtn, res_icon_btn_stop, res_icon_btn_stop_hover);

    update_time("0:00:00");
    update_btn_state();
}

void MainPage::update_btn_state() {
    ui->overBtn->setHidden(!m_isStarted);
    if (m_isStarted) {
        if (m_isRecording) set_widget_icon(ui->startBtn, res_icon_btn_pause, res_icon_btn_pause_hover);
        else set_widget_icon(ui->startBtn, res_icon_btn_resume, res_icon_btn_resume_hover);
    }
    else set_widget_icon(ui->startBtn, res_icon_btn_start, res_icon_btn_start_hover);
}

void MainPage::set_widget_icon(QWidget *qw, QString icon, QString hover) {

    qw->setStyleSheet(QString("\
        QPushButton {\
            qproperty-icon: url(' ');\
            qproperty-iconSize: 150px 42px;\
            border-image: url('%1');\
        }\
        QPushButton:hover {\
            border-image: url('%2');\
        }"
    ).arg(icon).arg(hover));
}

void MainPage::update_time(QString time) {
    ui->label_time->setText(time);
}

void MainPage::on_recordMenu_clicked()
{
    std::cout<<"in +++++++++++++++"<<std::endl;
}


void MainPage::on_fileBrowseBtn_clicked()
{
    QString dir = QDir::homePath();
    QString name = "record.txt";
    //QString fileName =
//    QFileDialog::getSaveFileName(this, tr("保存路径"), dir+"/"+name, tr("TXT (*.txt)"));
//    if(fileName.isNull())
//    {
//        return;
//    }
//    ui->filePathLineEdit->setText(fileName);

    m_page_output->OnBrowse();
    m_page_output->OnUpdateSuffixAndContainerFields();
    ui->filePathLineEdit->setText(m_page_output->m_lineedit_file->text());

}

void MainPage::on_miniBtn_clicked()
{
//    window()->showMinimized();
    OnShowHide();
}


void MainPage::on_maxBtn_clicked(bool checked) {
    if(checked) {
        window()->showMaximized();
        ui->maxBtn->setIcon(QIcon(":/originNormal.png"));
    } else {
        window()->showNormal();
        ui->maxBtn->setIcon(QIcon(":/maxNomal.png"));
    }
}

void MainPage::on_maxBtn_clicked()
{
    if(checked)
    {
        checked = false;
        on_maxBtn_clicked(checked);

    }else {
        checked = true;
        on_maxBtn_clicked(checked);
    }
}

static std::vector<QRect> GetScreenGeometries() {
    std::vector<QRect> screen_geometries;
#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
    for(QScreen *screen :  QApplication::screens()) {
        QRect geometry = screen->geometry();
        qreal ratio = screen->devicePixelRatio();
        screen_geometries.emplace_back(geometry.x(), geometry.y(), lrint((qreal) geometry.width() * ratio), lrint((qreal) geometry.height() * ratio));
    }
#else
    for(iw.width()nt i = 0; i < QApplication::desktop()->screenCount(); ++i) {
        screen_geometries.push_back(QApplication::desktop()->screenGeometry(i));
    }
#endif
    return screen_geometries;
}



void MainPage::OnIdentifyScreens() {
    OnStopIdentifyScreens();
    std::vector<QRect> screen_geometries = GetScreenGeometries();

    for(size_t i = 0; i < screen_geometries.size(); ++i) {
        QRect &rect = screen_geometries[i];
//        ScreenLabelWindow *label = new ScreenLabelWindow(this, tr("Screen %1", "This appears in the screen labels").arg(i + 1));
//        label->move(rect.x(), rect.y());
//        label->show();
//        m_screen_labels.push_back(label);
        QAction *tmpAction = new QAction(screensMenu);
        tmpAction->setText(tr("Screen %1", "This appears in the screen labels").arg(i + 1));
        screensMenu->addAction(tmpAction);
        //.setText(tr("Screen %1", "This appears in the screen labels").arg(i + 1)
    }
}


void MainPage::srcAudioMenu_func()
{
//    m_page_input->setAudioSrc(1);
    m_page_input->m_combobox_audio_backend->setCurrentIndex(1);
    m_page_output->m_combobox_audio_codec_av->setCurrentIndex(0);
    int src_count = m_page_input->m_combobox_pulseaudio_source->count();

    for(int i = 0; i < src_count; ++i)
    {
        QAction * tmpAction = new QAction(srcAudioGroup);
        QString title = m_page_input->m_combobox_pulseaudio_source->itemText(i);
        tmpAction->setData(i);
        tmpAction->setText(title);
        tmpAction->setCheckable(true);
        srcAudioMenu->addAction(tmpAction);
        srcAudioGroup->addAction(tmpAction);
        connect(tmpAction, &QAction::triggered, this ,&MainPage::srcAudio_slot);
    }
}

void MainPage::srcAudio_slot()
{
    QAction *act = qobject_cast<QAction *>(sender());
    m_page_input->m_checkbox_audio_enable->setChecked(true);
    QVariant data = act->data();
     int index = data.toInt();
    QFontMetrics metrix(act->text());
    QFontMetrics fontWidth(act->text());
    QString elidesText = fontWidth.elidedText(act->text(),Qt::ElideRight,150);
     ui->VolDynamicLabel->setText(elidesText);
     noAudiAction->setChecked(false);
     ui->micCheckbox->setCheckable(true);
     m_page_input->m_combobox_pulseaudio_source->setCurrentIndex(index);
}

void MainPage::identifyScreensFunc() {
    int item_count = m_page_input->m_combobox_screens->count();
    //default
    ui->recordDynamicLabel->setText(m_page_input->m_combobox_screens->itemText(0));
    m_page_input->m_combobox_screens->setCurrentIndex(0);
    m_page_input->OnUpdateVideoAreaFields();
    m_page_input->OnUpdateRecordingFrame();

    for(int i = 0; i < item_count; ++i)
    {
//        QAction *tmpAction = qobject_cast<QAction *>(sender());
        QAction *tmpAction = new QAction(screenGroup);
        QString title = m_page_input->m_combobox_screens->itemText(i);
        tmpAction->setData(i);
        tmpAction->setText(title);
        tmpAction->setCheckable(true);
//        connect(tmpAction, SIGNAL(MainPage::transClick(tmpAction)), this ,SLOT(&MainPage::screenIndexConnectFunc(tmpAction)));
        connect(tmpAction, SIGNAL(triggered()), this, SLOT(screenIndexConnectFunc()));
        screenGroup->addAction(tmpAction);
        screensMenu->addAction(tmpAction);
        //default

    }
}



void MainPage::screenIndexConnectFunc() {
    QAction *act = qobject_cast<QAction *>(sender());
    m_page_input->radio_area_screen->setChecked(true);

    if (act->isChecked()) {
        QVariant data = act->data();
        int index = data.toInt();
        std::cout << "jjjjjjjjjjjjjjjjjjjjjjjjjjj" << index << std::endl;
        ui->recordDynamicLabel->setText(act->text());
        m_page_input->m_combobox_screens->setCurrentIndex(index);
        m_page_input->OnUpdateVideoAreaFields();
        m_page_input->OnUpdateRecordingFrame();
    }

}


void MainPage::OnStopIdentifyScreens() {
//    for(unsigned int i = 0; i < m_screen_labels.size(); ++i) {
//        delete m_screen_labels[i];
//    }
//    m_screen_labels.clear();
    
}


//设置倒计时
void MainPage::on_startBtn_clicked() {
  //PageRecord::StartOutput();
    if(m_isCounted)
    {
        return;
    }
    QString path = ui->filePathLineEdit->text().left(ui->filePathLineEdit->text().lastIndexOf("/"));
    const QFileInfo info(path);
    if(ui->recordDynamicLabel->text().isEmpty()||
            ui->VolDynamicLabel->text().isEmpty()
            ||ui->typeDynamicLabel->text().isEmpty()
            ||ui->filePathLineEdit->text().isEmpty())
    {
        QMessageBox::about(NULL, "警告", "未选择必要选项");
        std::cout<<"wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww"<<std::endl;
        return;
    }else if(!(info.isWritable()))
    {
        QMessageBox::about(NULL, "警告", "所选择路径无写入权限");
    }
    else
    {
        if((1 == ptrsettingmenu->ui->checkBox_9->isChecked())&&(m_isCounted == false) && m_page_record->m_page_started == false)
        {
            m_isCounted = true;
            timeLabel = new QLabel();
            myMovie = new QMovie(":/icon_timer.gif");
            timeLabel->setMovie(myMovie);
            timeLabel->setScaledContents(true);
            timeLabel->setWindowFlags(Qt::FramelessWindowHint);

            //connect(myMovie,SIGNAL(frameChanged(int)),this,SLOT(onframeChanged(int)));
            connect(myMovie,SIGNAL(finished()),this,SLOT(onframeChanged()));
            myMovie->start();
            timeLabel->show();
            timeLabel->move((QApplication::desktop()->width() - timeLabel->width())/2,(QApplication::desktop()->height() - timeLabel->height())/2);

            if(1 == ptrsettingmenu->ui->checkBox_10->isChecked())
            {
                OnHide();
            }
            //solve moive and video sycn record
            QEventLoop loop;
            QTimer::singleShot(3000,&loop,SLOT(quit()));
            loop.exec();
            m_isCounted = false;
        }

        if(ui->micCheckbox->isChecked())
        {
            QProcess::execute("pactl load-module module-loopback");

        }else{
            QProcess::execute("pactl unload-module module-loopback");
        }

        if(1 == ptrsettingmenu->ui->checkBox_10->isChecked())
        {
            if( (m_isStarted == false)||(m_isRecording == false))
                OnHide();
        }
        m_isStarted = true;
        m_isRecording = !m_isRecording;
        update_btn_state();
    //  PageRecord::StartOutput();
        bool isZoom = ui->zoomCheckBox->isChecked();
        m_h = ui->hSpinbox->text().toInt();
        m_l = ui->wSpinBox->text().toInt();
        if(isZoom)
        {
            m_page_input->m_checkbox_scale->setChecked(true);
            m_page_input->m_spinbox_video_scaled_h->setValue(m_h);
            m_page_input->m_spinbox_video_scaled_w->setValue(m_l);
            m_page_input->OnUpdateVideoScaleFields();
        }else if(m_h != 0 || m_l != 0)
        {
            m_page_input->m_checkbox_scale->setChecked(false);
            m_page_input->m_combobox_screens->setCurrentIndex(0);
            m_page_input->OnUpdateVideoAreaFields();
            m_page_input->OnUpdateRecordingFrame();
        }
        QProcess *pro = new QProcess;
        if(ptrsettingmenu->ui->camCheckBox->isChecked())
        {

            pro->start("cheese");
        }
        m_page_input->OnUpdateAudioFields();
        m_page_input->OnUpdatePulseAudioSources();

        m_page_record->StartPage();
        m_page_output->OnUpdateSuffixAndContainerFields();
        m_page_record->OnRecordStartPause();
    }
}


void MainPage::on_overBtn_clicked() {
    cout<<"on_overBtn_clicked"<<endl;
    m_isStarted = false;
    m_isRecording = false;
    update_btn_state();
    QProcess::execute("pkill cheese");
    m_page_record->OnRecordSave();

    cout<<"on_overBtn_clicked container_avname\t"<<m_page_record->m_output_settings.container_avname.toStdString()<<endl;
}

void MainPage::send(QString data)
{
    emit sendData(data);
}

void MainPage::OnShow() {

//    if(IsBusy())
//        return;
//    if(isVisible())
//        return;
    show();
    if(!m_old_geometry.isNull()) {
        setGeometry(m_old_geometry);
        m_old_geometry = QRect();
    }
    m_page_record->UpdateShowHide();
}

void MainPage::OnHide() {
    if(IsBusy())
        return;
    if(!isVisible())
        return;
    m_old_geometry = geometry();
    hide();
    m_page_record->UpdateShowHide();
}

void MainPage::OnShowHide() {
    Logger::LogInfo("OnShowHide");
    if(isVisible()) {
        OnHide();
    } else {
        OnShow();
    }
}

void MainPage::OnSysTrayActivated(QSystemTrayIcon::ActivationReason reason) {
    if(reason == QSystemTrayIcon::Trigger || reason == QSystemTrayIcon::DoubleClick) {
        OnShowHide();
    }
}

//void MainPage::Record_func()
//{
//    //todo: identify the index of screens.
//    std::vector<QRect> screen_geometries = GetScreenGeometries();
//    QRect rect;
//    if(sc > 0 && sc <= (int) screen_geometries.size()) {
//        rect = screen_geometries[sc - 1];
//    } else {
//        rect = CombineScreenGeometries(screen_geometries);
//    }
//    SetVideoX(rect.left());
//    SetVideoY(rect.top());
//    SetVideoW(rect.width());
//    SetVideoH(rect.height());
//}

void MainPage::fixSizeRecord_func()
{

    ui->recordDynamicLabel->setText("区域录制");
    if(sizeAction->isChecked())
    {
        fullAction->setDisabled(true);
        m_page_input->radio_area_fixed->setChecked(true);
        m_page_input->OnStartSelectRectangle();
        m_page_input->OnUpdateVideoAreaFields();
        m_page_input->OnUpdateRecordingFrame();
    }else
    {
        fullAction->setDisabled(false);
        ui->recordDynamicLabel->setText("");

    }

}

void MainPage::GoPageStart() {
//    if(m_page_welcome->GetSkipPage()) {
//        m_stacked_layout->setCurrentWidget(m_page_input);
//    } else {
//        m_stacked_layout->setCurrentWidget(m_page_welcome);
//    }
}
void MainPage::GoPageWelcome() {
//    m_stacked_layout->setCurrentWidget(m_page_welcome);
}
void MainPage::GoPageInput() {
    m_stacked_layout->setCurrentWidget(m_page_input);
}
void MainPage::GoPageOutput() {
    m_stacked_layout->setCurrentWidget(m_page_output);
    m_page_output->StartPage();
}
void MainPage::GoPageRecord() {
    //m_stacked_layout->setCurrentWidget(m_page_record);
    m_page_record->StartPage();
}
void MainPage::GoPageDone() {
//    m_stacked_layout->setCurrentWidget(m_page_done);
}




bool MainPage::IsBusy() {
    return (QApplication::activeModalWidget() != NULL || QApplication::activePopupWidget() != NULL);
}

bool MainPage::Validate() {
    if(!m_page_input->Validate())
        return false;
    if(!m_page_output->Validate())
        return false;
    return true;
}



void MainPage::on_zoomCheckBox_stateChanged(int state) {
    ui->frame_resize->setEnabled(state == 2);
}

void MainPage::mouseMoveEvent(QMouseEvent *event) {
    if (pressed)
    {
        QPoint deltaPos = event->globalPos() - current;
        move(deltaPos);
        std::cout<<this->pos().x()<<std::endl;
    }
}

bool MainPage::eventFilter(QObject *object, QEvent *event) {
//    if (event->type() == QEvent::Show && object == ui->recordMenu->menu())
//    {
//        int menu_x_pox = ui->recordMenu->menu()->pos().x();
//        int menu_width = ui->recordMenu->menu()->size().width();
//        int button_width = ui->recordMenu->size().width();
//        QPoint pos = QPoint(menu_x_pox - menu_width + button_width, ui->recordMenu->menu()->pos().y()/2);

//        ui->recordMenu->menu()->move(pos);
//        return true;
//    }

    if (object == ui->titleFrame && event->type() == QEvent::MouseButtonPress) {
        QMouseEvent* mouseEvent = (QMouseEvent*)event;
        if (pressed == false) {
            current = mouseEvent->globalPos() - this->pos();
        }
        pressed = true;
        return true;
    }
    if (object == ui->titleFrame && event->type() == QEvent::MouseButtonRelease) {
        pressed = false;
        return true;
    }


    return false;
}

void MainPage::mp4TypeFunc() {
    m_page_output->m_combobox_container->setCurrentIndex(1);
    m_page_output->OnUpdateSuffixAndContainerFields();
    m_page_output->m_combobox_video_codec->setCurrentIndex(0);
    m_page_output->m_combobox_audio_codec->setCurrentIndex(0);
    ui->typeDynamicLabel->setText("MP4");
    ui->filePathLineEdit->setText(m_page_output->m_lineedit_file->text());
}

void MainPage::mkvTypeFunc()
{
    m_page_output->m_combobox_container->setCurrentIndex(0);
    m_page_output->OnUpdateSuffixAndContainerFields();
    ui->filePathLineEdit->setText(m_page_output->m_lineedit_file->text());
    ui->typeDynamicLabel->setText("MKV");

}

void MainPage::webmTypeFunc()
{
    m_page_output->m_combobox_container->setCurrentIndex(2);
    m_page_output->m_combobox_video_codec->setCurrentIndex(1);
    m_page_output->m_combobox_audio_codec->setCurrentIndex(0);

    m_page_output->OnUpdateSuffixAndContainerFields();
    ui->filePathLineEdit->setText(m_page_output->m_lineedit_file->text());
    ui->typeDynamicLabel->setText("Webm");


}

void MainPage::oggTypeFunc()
{
    m_page_output->m_combobox_container->setCurrentIndex(3);
    m_page_output->m_combobox_video_codec->setCurrentIndex(2);
    m_page_output->m_combobox_audio_codec->setCurrentIndex(0);
    m_page_output->OnUpdateSuffixAndContainerFields();
    ui->filePathLineEdit->setText(m_page_output->m_lineedit_file->text());
    m_page_input->SetVideoFrameRate(16);
    ui->typeDynamicLabel->setText("Ogg");
}

void MainPage::selectRecFunc()
{
    m_page_input->OnStartSelectRectangle();
}

void MainPage::on_recordCursorCheckBox_clicked()
{
    PageInput *page_input = this->GetPageInput();

    if(ui->recordCursorCheckBox->isChecked())
    {
        std::cout<<"cursor on"<<std::endl;
         page_input->SetVideoRecordCursor(true);
    }else
    {
        page_input->SetVideoRecordCursor(false);
    }
}

void MainPage::noAudioFunc()
{
    m_page_input->m_combobox_audio_backend->setCurrentIndex(1);
    if(noAudiAction->isChecked())
    {
        std::cout<<"no voice"<<std::endl;
        m_page_input->m_checkbox_audio_enable->setChecked(false);
        m_page_input->m_checkbox_jack_connect_system_capture->setChecked(false);
        m_page_input->m_checkbox_jack_connect_system_playback->setChecked(false);

        auto actions = srcAudioMenu->findChildren<QAction*>();
        for (QAction *action : actions)
        {
            action->setChecked(false);
        }
        sysmicAction->setDisabled(true);
        sysAudiAction->setDisabled(true);
        ui->micCheckbox->setChecked(false);
        ui->micCheckbox->setCheckable(false);
        ui->VolDynamicLabel->setText("不录音");
    }else
    {
        std::cout<<"no voice"<<std::endl;

        sysmicAction->setEnabled(true);
        sysAudiAction->setEnabled(true);
        m_page_input->m_checkbox_audio_enable->setChecked(true);
        m_page_input->m_checkbox_jack_connect_system_capture->setChecked(true);
        m_page_input->m_checkbox_jack_connect_system_playback->setChecked(true);
        if(sysAudiAction->isChecked())
        {
            m_page_input->m_checkbox_jack_connect_system_capture->setChecked(true);
        }
        if(sysmicAction->isChecked())
        {
            m_page_input->m_checkbox_jack_connect_system_playback->setChecked(true);
        }
        ui->VolDynamicLabel->setText("");

    }
}

void MainPage::sysAudioFunc()
{
    m_page_input->m_combobox_audio_backend->setCurrentIndex(2);

    if(sysAudiAction->isChecked())
    {
//        m_page_input->m_checkbox_jack_connect_system_capture->setChecked(false);
    }else
    {
        m_page_input->m_combobox_pulseaudio_source->setCurrentIndex(2);

    }
}

void MainPage::micAudioFunc()
{
    m_page_input->m_combobox_audio_backend->setCurrentIndex(2);
    int indexCount = m_page_input->m_combobox_pulseaudio_source->count();

    if(sysmicAction->isChecked())
    {
//        m_page_input->m_checkbox_jack_connect_system_playback->setChecked(false);
    }else
    {
        m_page_input->m_combobox_pulseaudio_source->setCurrentIndex(indexCount-1);

    }
}

void MainPage::separateVideoFunc()
{
    if(!ui->segmentCheckBox->isChecked())
    {
        std::cout<<"in separate )00000000000000000000000000000"<<std::endl;
        m_page_output->m_checkbox_separate_files->setChecked(true);
//        m_page_output->m_checkbox_add_timestamp->setChecked(true);
        m_page_record->m_add_timestamp = true;
//        m_page_output->OnUpdateSuffixAndContainerFields();
//        ui->filePathLineEdit->setText(m_page_output->m_lineedit_file->text());
    }else
    {
        m_page_output->m_checkbox_separate_files->setChecked(false);
        m_page_record->m_add_timestamp = false;
    }
}

/************************************ cjx ****************************/
//倒计时
void MainPage::on_button_timer()
{
    Logger::LogInfo("on_button_timer");
    a=4;
    recording_start_timer = new QTimer(this);
    connect(recording_start_timer,&QTimer::timeout,this,[=](){
        MainPage::showtimelimit();
    });
    recording_start_timer->start(1000);

}

void MainPage::showtimelimit()
{
    if(a != 0)
    {
        std::cout<<"a = "<<a<<endl;
        int b = a - 1;
        a--;
        std::cout<<"a = "<<a<<endl;
        //timeLimit->setNum(a);
    }
    else
    {
        //timeLimit->setVisible(false);
        //倒计时启动后，关闭窗口
        this->close();
    }
}

void MainPage::setting_click()
{
    Logger::LogInfo("==================== " + Logger::tr("setting_click") + " ====================");
    //setting_menu_new *settingmenu = new setting_menu_new();
    //settingmenu->StartPage();

    QSettings settings(CommandLineOptions::GetSettingsFile(), QSettings::IniFormat);
    //settingmenu->OnUpdateHotkeyFields();

    //变更需求隐藏窗口
//    ptrsettingmenu->ui->label_3->setVisible(false);
//    ptrsettingmenu->ui->label_16->setVisible(false);
//    ptrsettingmenu->ui->lineEdit_3->setVisible(false);
//    ptrsettingmenu->ui->horizontalSlider_2->setVisible(false);
//    ptrsettingmenu->ui->lineEdit_slider->setVisible(false);
    //隐藏标题栏
    //ptrsettingmenu->setWindowFlags(Qt::FramelessWindowHint);

    ptrsettingmenu->ui->listWidget->setGridSize(QSize(0,24));
    ptrsettingmenu->ui->listWidget->setSpacing(10);

    QRect rect = this->geometry();
    int x = rect.x() + rect.width()/6;
    int y = rect.y() + rect.height()/4;
    ptrsettingmenu->move(x,y);
    //设置视频参数-固定码率系数：
//    ptrsettingmenu->ui->horizontalSlider_2->setRange(0,50);
//    ptrsettingmenu->ui->horizontalSlider_2->setPageStep(15*16);
//    ptrsettingmenu->ui->horizontalSlider_2->setTickPosition(ptrsettingmenu->ui->horizontalSlider_2->TicksRight);

//    connect(ptrsettingmenu->ui->horizontalSlider_2,&QSlider::valueChanged,[=](int val){
//        ptrsettingmenu->ui->lineEdit_slider->setText(QString::number(val));
//    });
        //ptrsettingmenu->setGeometry(300,300,450,450);
    ptrsettingmenu->LoadSettings(&settings);
    ptrsettingmenu->exec();
}

void MainPage::about_click()
{
    Logger::LogInfo("==================== " + Logger::tr("about_click") + " ====================");
    aboutDialog *aboutdialog = new aboutDialog();
    QRect rect = this->geometry();
    int x = rect.x() + rect.width()/6;
    int y = rect.y() + rect.height()/4;
    aboutdialog->move(x,y);
    aboutdialog->exec();

}

void MainPage::log_click()
{
    Logger::LogInfo("==================== " + Logger::tr("log_click") + " ====================");
    //Log *loginfo = new Log();
    //loginfo->exec();
    QRect rect = this->geometry();
    int x = rect.x() + rect.width()/6;
    int y = rect.y() + rect.height()/4;
    ptrlog->move(x,y);
    ptrlog->exec();

}


void MainPage::LoadSettings() {

    QSettings settings(CommandLineOptions::GetSettingsFile(), QSettings::IniFormat);

    SetNVidiaDisableFlipping(StringToEnum(settings.value("global/nvidia_disable_flipping", QString()).toString(), NVIDIA_DISABLE_FLIPPING_ASK));

    // m_page_welcome->LoadSettings(&settings);
    // m_page_input->LoadSettings(&settings);
    // m_page_output->LoadSettings(&settings);
    // m_page_record->LoadSettings(&settings);
    ptrsettingmenu->LoadSettings(&settings);

}

void MainPage::SaveSettings() {

    QSettings settings(CommandLineOptions::GetSettingsFile(), QSettings::IniFormat);
    settings.clear();

    settings.setValue("global/nvidia_disable_flipping", EnumToString(GetNVidiaDisableFlipping()));

    // m_page_welcome->SaveSettings(&settings);
    // m_page_input->SaveSettings(&settings);
    // m_page_output->SaveSettings(&settings);
    // m_page_record->SaveSettings(&settings);
        ptrsettingmenu->SaveSettings(&settings);

}

void MainPage::Quit() {
    SaveSettings();
    if(m_nvidia_reenable_flipping) {
        NVidiaSetFlipping(true);
    }
    QApplication::quit();
}

//未使用，使用自定义关闭按钮
void MainPage::closeEvent(QCloseEvent* event) {
    Logger::LogInfo("closewindow*************");


    //SettingMenu *settingmenu = new SettingMenu();
    bool exitwindow = ptrsettingmenu->is_exit_windows();
    bool mintray = ptrsettingmenu->is_min_tray();
    bool hide_main = ptrsettingmenu->hide();
    cout<<"exitwindow\t"<<exitwindow<<endl;
    cout<<"mintray\t"<<mintray<<endl;
    cout<<"hide_main\t"<<hide_main<<endl;

    if(1 == exitwindow)
    {
        Logger::LogInfo("setting_ptr->ui->radioButton_4->isChecked()");
        if(m_isStarted)
        {
            std::cout<<"noooooooooooooooooo"<<std::endl;
        }
        //退出
        event->accept();
        Quit();
    }

    if(1 == mintray)
    {
        Logger::LogInfo("setting_ptr->ui->radioButton_3->isChecked()");
        //最小化托盘
        event->accept();
        Quit();

    }
}

//点击关闭
void MainPage::on_closeBtn_clicked()
{
    Logger::LogInfo("closewindow*************");


    //SettingMenu *settingmenu = new SettingMenu();
    bool exitwindow = ptrsettingmenu->is_exit_windows();
    bool mintray = ptrsettingmenu->is_min_tray();
    bool hide_main = ptrsettingmenu->hide();
    cout<<"exitwindow\t"<<exitwindow<<endl;
    cout<<"mintray\t"<<mintray<<endl;
    cout<<"hide_main\t"<<hide_main<<endl;

    if(1 == exitwindow)
    {
        Logger::LogInfo("setting_ptr->ui->radioButton_4->isChecked()");
        //退出
        QApplication *app;
        app->exit();
    }

    if(1 == mintray)
    {
        Logger::LogInfo("setting_ptr->ui->radioButton_3->isChecked()");
        //最小化托盘
        OnShowHide();
    }
}

void MainPage::syncAudioFunc()
{
//    QProcess::execute("pactl load-module module-loopback");
//    m_page_input->m_checkbox_audio_enable->setChecked(true);
//    if (syncAudioAction->isChecked())
//    {
//        m_page_input->m_combobox_pulseaudio_source->setCurrentIndex(0);
//        ui->VolDynamicLabel->setText("同时录制");
//    }
     ui->VolDynamicLabel->setText("同时录制");
}
QString MainPage::GetPathInfo()
{
    cout<<"GetPathInfo"<<endl;
    QString pathinfo= ui->filePathLineEdit->text();
    if(NULL == pathinfo)
    {
        return NULL;
    }
    cout<<"pathinfo\t"<<pathinfo.toStdString();
    return pathinfo;
}

void MainPage::SetFormatEnabel(int video, int audio)
{
    cout<<"SetFormatEnabel"<<endl;

    mkvAction->setDisabled(false);
    mp4Action->setDisabled(false);
    webmAction->setDisabled(false);
    oggAction->setDisabled(false);
    hintAciton->setDisabled(true);

    //video = other
    if(video == ptrsettingmenu->VIDEO_CODEC_OTHER)
    {
        mkvAction->setDisabled(true);
        mp4Action->setDisabled(true);
        webmAction->setDisabled(true);
        oggAction->setDisabled(true);
    }

    if(video == ptrsettingmenu->VIDEO_CODEC_THEORA)
    {
        mp4Action->setDisabled(true);
        webmAction->setDisabled(true);
    }

    if(video == ptrsettingmenu->VIDEO_CODEC_H264)
    {
        webmAction->setDisabled(true);
        oggAction->setDisabled(true);
    }

    if(audio == ptrsettingmenu->AUDIO_CODEC_OTHER)
    {
        mkvAction->setDisabled(true);
        mp4Action->setDisabled(true);
        webmAction->setDisabled(true);
        oggAction->setDisabled(true);
    }

    if(audio == ptrsettingmenu->AUDIO_CODEC_UNCOMPRESSED)
    {
        mp4Action->setDisabled(true);
        webmAction->setDisabled(true);
        oggAction->setDisabled(true);
    }
    if(audio == ptrsettingmenu->AUDIO_CODEC_AAC)
    {
        webmAction->setDisabled(true);
        oggAction->setDisabled(true);
    }
    if(audio == ptrsettingmenu->AUDIO_CODEC_AAC && video == ptrsettingmenu->VIDEO_CODEC_H264)
    {
    }
    //bug369841
    ui->typeDynamicLabel->setText("");
}
//dao ji shi
void MainPage::onframeChanged()
{

        myMovie->stop();
        timeLabel->close();

}



void MainPage::on_segmentCheckBox_clicked(bool checked)
{
    if(checked)
    {
        std::cout<<"in sepatator 00000000000000000000000000000000000000"<<std::endl;
        m_page_output->m_checkbox_separate_files->setChecked(true);
    }else
    {
        m_page_output->m_checkbox_separate_files->setChecked(false);
    }
}

void MainPage::highQuaFunc()
{
    m_page_output->m_slider_h264_crf->setValue(1);
    m_page_output->m_combobox_h264_preset->setCurrentIndex(0);
    m_page_output->m_combobox_vp8_cpu_used->setCurrentIndex(0);
    m_page_output->SetVideoKBitRate(8000);
    ui->quaDynamicLabel->setText("高画质");
}

void MainPage::midQuaFunc()
{
    m_page_output->m_slider_h264_crf->setValue(30);
    m_page_output->m_combobox_h264_preset->setCurrentIndex(0);
    m_page_output->m_combobox_vp8_cpu_used->setCurrentIndex(2);
    m_page_output->SetVideoKBitRate(4000);
    ui->quaDynamicLabel->setText("中等画质");

}

void MainPage::lowQuaFunc()
{

    m_page_output->m_slider_h264_crf->setValue(50);
    m_page_output->m_combobox_h264_preset->setCurrentIndex(0);
    m_page_output->m_combobox_vp8_cpu_used->setCurrentIndex(4);
    m_page_output->SetVideoKBitRate(2000);
    ui->quaDynamicLabel->setText("低画质");

}

int MainPage::GetVideoCodecIndex()
{
    int videoindex = ptrsettingmenu->GetVideoCodecIndex();
    cout<<"videocodeindex\t"<<videoindex<<endl;
    return videoindex;
}

int MainPage::GetAudioCodecIndex()
{
    int audioindex = ptrsettingmenu->GetAudioCodecIndex();
    cout<<"audiocodeindex\t"<<audioindex<<endl;
    return audioindex;
}


void MainPage::changehint()
{
    cout<<"changehint"<<endl;
    QPalette pe;
    pe.setColor(QPalette::WindowText,Qt::red);
    ui->attention->setPalette(pe);
    QEventLoop loop;
    QTimer::singleShot(3000,&loop,SLOT(quit()));
    loop.exec();
    ui->attention->setVisible(false);
}

void MainPage::setvideopara(int &super,int &cpu,int &frame, QString &video_name)
{

    frame = ptrsettingmenu->GetVideoAllowFrameSkipping();
    video_name = ptrsettingmenu->GetVideoCodecAVName();
}

void MainPage::setaudiopara(int &sample,QString &audio_codec_avname,bool &is_voice)
{
    sample = ptrsettingmenu->GetAudioKBitRate();
    audio_codec_avname = ptrsettingmenu->GetAudioCodecAVName();
    is_voice = noAudiAction->isChecked();
}

//初始化音视频参数以及格式
void MainPage::Init_SetFormatEnabel(int video, int audio)
{
    mkvAction->setDisabled(false);
    mp4Action->setDisabled(false);
    webmAction->setDisabled(false);
    oggAction->setDisabled(false);
    hintAciton->setDisabled(true);

    //video = other
    if(video == ptrsettingmenu->VIDEO_CODEC_OTHER)
    {
        mkvAction->setDisabled(true);
        mp4Action->setDisabled(true);
        webmAction->setDisabled(true);
        oggAction->setDisabled(true);
    }

    if(video == ptrsettingmenu->VIDEO_CODEC_THEORA)
    {
        mp4Action->setDisabled(true);
        webmAction->setDisabled(true);
        mkvAction->setEnabled(true);
        oggAction->setEnabled(true);
    }

    if(video == ptrsettingmenu->VIDEO_CODEC_H264)
    {
        webmAction->setDisabled(true);
        oggAction->setDisabled(true);
    }

    if(audio == ptrsettingmenu->AUDIO_CODEC_OTHER)
    {
        mkvAction->setDisabled(true);
        mp4Action->setDisabled(true);
        webmAction->setDisabled(true);
        oggAction->setDisabled(true);
    }

    if(audio == ptrsettingmenu->AUDIO_CODEC_UNCOMPRESSED)
    {
        mp4Action->setDisabled(true);
        webmAction->setDisabled(true);
        oggAction->setDisabled(true);
    }
    if(audio == ptrsettingmenu->AUDIO_CODEC_AAC)
    {
        webmAction->setDisabled(true);
        oggAction->setDisabled(true);
    }

}

void MainPage::on_micCheckbox_clicked(bool checked)
{
    if(checked)
    {
        noAudiAction->setChecked(false);
    }
}

#ifndef ABOUTDIALOG_H
#define ABOUTDIALOG_H

#include <QDialog>

namespace Ui {
class aboutDialog;
}

class aboutDialog : public QDialog
{
    Q_OBJECT

public:
    explicit aboutDialog(QWidget *parent = nullptr);
    ~aboutDialog();

protected:
    bool eventFilter(QObject *o, QEvent *e);

private:
    Ui::aboutDialog *ui;

    QPoint m_movePos;
    bool m_pressed;
};

#endif // ABOUTDIALOG_H

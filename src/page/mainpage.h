#ifndef MAINPAGE_H
#define MAINPAGE_H

#pragma once
#include "Global.h"
#include "aboutdialog.h"
#include "LogInfo.h"
#include "SettingMenu.h"
#include <QThread>


class PageWelcome;
class PageInput;
class PageOutput;
class PageRecord;
class PageDone;
namespace Ui {
class MainPage;
}

class MainPage : public QMainWindow
{
    Q_OBJECT

public:
    enum enum_audio_backend {
        AUDIO_BACKEND_ALSA,
        AUDIO_BACKEND_PULSEAUDIO,
        AUDIO_BACKEND_JACK,
        AUDIO_BACKEND_COUNT // must be last
    };
    enum enum_nvidia_disable_flipping {
        NVIDIA_DISABLE_FLIPPING_ASK,
        NVIDIA_DISABLE_FLIPPING_YES,
        NVIDIA_DISABLE_FLIPPING_NO,
        NVIDIA_DISABLE_FLIPPING_COUNT // must be last
    };


public:
    static const QString WINDOW_CAPTION;

public:
    explicit MainPage(QWidget *parent = 0);
    ~MainPage();

//    void LoadSettings();
//    void SaveSettings();

    bool IsBusy();
    bool Validate();
//    void Quit();
    inline PageInput* GetPageInput() { return m_page_input; }
    inline PageOutput* GetPageOutput() { return m_page_output; }

    inline enum_nvidia_disable_flipping GetNVidiaDisableFlipping() { return m_nvidia_disable_flipping; }

    inline void SetNVidiaDisableFlipping(enum_nvidia_disable_flipping flipping) { m_nvidia_disable_flipping = (enum_nvidia_disable_flipping) clamp((unsigned int) flipping, 0u, (unsigned int) NVIDIA_DISABLE_FLIPPING_COUNT - 1); }

    void init_old_logic();
    void init_footer();
    void update_btn_state();
    void set_widget_icon(QWidget *qw, QString icon, QString hover);
    void update_time(QString time);

    void mouseMoveEvent(QMouseEvent *event);
    bool eventFilter(QObject *object, QEvent *event);
    QPoint current;
    bool pressed;
    bool m_isRecording;
    bool m_isStarted;
    bool m_isCounted = false;

public slots:
    void on_recordMenu_clicked();
    void on_fileBrowseBtn_clicked();
    void on_miniBtn_clicked();
    void on_maxBtn_clicked(bool checked);
    void on_maxBtn_clicked();
    void on_closeBtn_clicked();
    void OnIdentifyScreens();
    void OnStopIdentifyScreens();
    void on_startBtn_clicked();
    void srcAudio_slot();
    void on_overBtn_clicked();

//    void fullRecord_func();
//    void fixSizeRecord_func();

    void OnShow();
    void OnHide();
    void OnShowHide();
    void OnSysTrayActivated(QSystemTrayIcon::ActivationReason reason);

    void on_zoomCheckBox_stateChanged(int);

    void GoPageStart();
    void GoPageWelcome();
    void GoPageInput();
    void GoPageOutput();
    void GoPageRecord();
    void fixSizeRecord_func();
    void GoPageDone();
    void mp4TypeFunc();
    void mkvTypeFunc();
    void webmTypeFunc();
    void oggTypeFunc();
    void noAudioFunc();
    void sysAudioFunc();
    void micAudioFunc();
    void highQuaFunc();
    void midQuaFunc();
    void lowQuaFunc();
    void identifyScreensFunc();
    void screenIndexConnectFunc();
    void separateVideoFunc();
    void syncAudioFunc();



    void selectRecFunc();

//    inline void SetVideoX(unsigned int x) { m_spinbox_video_x->setValue(x); }
//    inline void SetVideoY(unsigned int y) { m_spinbox_video_y->setValue(y); }
//    inline void SetVideoW(unsigned int w) { m_spinbox_video_w->setValue(w); }
//    inline void SetVideoH(unsigned int h) { m_spinbox_video_h->setValue(h); }
//    inline void SetVideoFrameRate(unsigned int frame_rate) { m_spinbox_video_frame_rate->setValue(frame_rate); }
//    inline void SetVideoScalingEnabled(bool enable) { m_checkbox_scale->setChecked(enable); }
//    inline void SetVideoScaledW(unsigned int scaled_w) { m_spinbox_video_scaled_w->setValue(scaled_w); }
//    inline void SetVideoScaledH(unsigned int scaled_h) { m_spinbox_video_scaled_h->setValue(scaled_h); }
//    inline void SetVideoRecordCursor(bool show) { m_checkbox_record_cursor->setChecked(show); }
//    inline void SetAudioEnabled(bool enable) { m_checkbox_audio_enable->setChecked(enable); }
//    inline void SetAudioBackend(enum_audio_backend backend) { m_combobox_audio_backend->setCurrentIndex(clamp((int) backend, 0, AUDIO_BACKEND_COUNT - 1)); }

public:
    Ui::MainPage *ui;

    bool checked = false;
    PageRecord *pr;
    QMenu *recMenu;
    QMenu *screensMenu;
    QMenu *audiMenu;
    QMenu *volMenu;
    QMenu *typeMenu;
    QMenu *quaMenu;
    QMenu *setMenu;
    QMenu *srcAudioMenu;
    QActionGroup *srcAudioGroup;
    QActionGroup *quaGroup;
    QActionGroup *audiGroup;
    QActionGroup *screenGroup;

    QAction *highAction;
    QAction *midAction;
    QAction *lowAction;
    QAction *setAction;
    QAction *logAction;
    QAction *aboutAction;
    QAction *noAudiAction;
    QAction *sysAudiAction;
    QAction *sysmicAction;
    QAction *sepAction;
    QAction *sizeAction;
    QAction *openGlAction;
    QAction *fullAction;
    QAction *alsaAudiAction;
    QAction *pulseAudiAction;
    QAction *jackAudiAction;
    QAction *mkvAction;
    QAction *mp4Action;
    QAction *webmAction;
    QAction *oggAction;
    QAction *hintAciton;
    QAction *srcAudioAction;
    QAction *syncAudioAction;
    bool isAudioInput;
    int m_h;
    int m_l;

    // from MainWindow.h
    enum_nvidia_disable_flipping m_nvidia_disable_flipping;
    bool m_nvidia_reenable_flipping;

    QRect m_old_geometry;

    QStackedLayout *m_stacked_layout;
    PageWelcome *m_page_welcome;
    PageInput *m_page_input;
    PageOutput *m_page_output;
    PageRecord *m_page_record;
    PageDone *m_page_done;

    // from PageInput.h
//    QLabel *m_label_video_x, *m_label_video_y, *m_label_video_w, *m_label_video_h;
//    QSpinBoxWithSignal *m_spinbox_video_x, *m_spinbox_video_y, *m_spinbox_video_w, *m_spinbox_video_h;
//    QSpinBox *m_spinbox_video_frame_rate;
//    QCheckBox *m_checkbox_scale;
//    QLabel *m_label_video_scaled_w, *m_label_video_scaled_h;
//    QSpinBox *m_spinbox_video_scaled_w, *m_spinbox_video_scaled_h;
//    QCheckBox *m_checkbox_record_cursor;

//    QCheckBox *m_checkbox_audio_enable;
//    QLabel *m_label_audio_backend;
//    QComboBox *m_combobox_audio_backend;


signals:
    void transClick(QAction *act);

private slots:
    void on_recordCursorCheckBox_clicked();

/************************************* cjx ****************************/
    
//    void on_segmentCheckBox_clicked();

    void on_segmentCheckBox_clicked(bool checked);

\
    void on_micCheckbox_clicked(bool checked);

public:
    void setting_click();
    void about_click();
    void log_click();
    void LoadSettings();
    void showtimelimit();
    void menuSlot();
    void on_button_timer();
    void Quit();
    void SaveSettings();
    QLabel* timeLabel;
    QMovie* myMovie;
    QString GetPathInfo();
    void SetFormatEnabel(int video, int audio);
    void Init_SetFormatEnabel(int video, int audio);
    void srcAudioMenu_func();
    void send(QString data);
    int GetVideoCodecIndex();
    int GetAudioCodecIndex();
    void setvideopara(int &super,int &cpu,int &frame, QString &video_name);
    void setaudiopara(int &sample,QString &audio_codec_avname,bool &is_voice);

protected:
	virtual void closeEvent(QCloseEvent* event) override;
private:
    aboutDialog* ptraboutdialog;
    LogInfo* ptrlog;
    SettingMenu* ptrsettingmenu;
	int a;
    QTimer *recording_start_timer;

public slots:
    void onframeChanged();
    void changehint();
signals:
    void sendData(QString data);

};

#endif // MAINPAGE_H

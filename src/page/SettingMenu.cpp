#include "SettingMenu.h"
#include "ui_SettingMenu.h"

#include <QCheckBox>
#include <QPushButton>
#include <QComboBox>
#include "Logger.h"
#include "AVWrapper.h"
#include "VideoEncoder.h"
#include "AudioEncoder.h"
#include "EnumStrings.h"
#include "HotkeyListener.h"
#include "mainpage.h"
#include "CommandLineOptions.h"
#include <iostream>
using namespace std;

ENUMSTRINGS(SettingMenu::enum_container) = {
    {SettingMenu::CONTAINER_MKV, "mkv"},
    {SettingMenu::CONTAINER_MP4, "mp4"},
    {SettingMenu::CONTAINER_WEBM, "webm"},
    {SettingMenu::CONTAINER_OGG, "ogg"},
    {SettingMenu::CONTAINER_OTHER, "other"},
};
ENUMSTRINGS(SettingMenu::enum_video_codec) = {
    {SettingMenu::VIDEO_CODEC_H264, "h264"},
//    {SettingMenu::VIDEO_CODEC_VP8, "vp8"},
    {SettingMenu::VIDEO_CODEC_THEORA, "theora"},
    {SettingMenu::VIDEO_CODEC_OTHER, "other"},
};
ENUMSTRINGS(SettingMenu::enum_audio_codec) = {
    {SettingMenu::AUDIO_CODEC_VORBIS, "vorbis"},
    {SettingMenu::AUDIO_CODEC_MP3, "mp3"},
    {SettingMenu::AUDIO_CODEC_AAC, "aac"},
    {SettingMenu::AUDIO_CODEC_UNCOMPRESSED, "uncompressed"},
    {SettingMenu::AUDIO_CODEC_OTHER, "other"},
};
ENUMSTRINGS(SettingMenu::enum_h264_preset) = {
    {SettingMenu::H264_PRESET_ULTRAFAST, "ultrafast"},
    {SettingMenu::H264_PRESET_SUPERFAST, "superfast"},
    {SettingMenu::H264_PRESET_VERYFAST, "veryfast"},
    {SettingMenu::H264_PRESET_FASTER, "faster"},
    {SettingMenu::H264_PRESET_FAST, "fast"},
    {SettingMenu::H264_PRESET_MEDIUM, "medium"},
    {SettingMenu::H264_PRESET_SLOW, "slow"},
    {SettingMenu::H264_PRESET_SLOWER, "slower"},
    {SettingMenu::H264_PRESET_VERYSLOW, "veryslow"},
    {SettingMenu::H264_PRESET_PLACEBO, "placebo"},
};

SettingMenu::SettingMenu(MainPage *parent) :
    QDialog(),
    ui(new Ui::SettingMenu)
{
        Logger::LogInfo("==================== " + Logger::tr("enter into SettingMenu") + " ====================");
    ui->setupUi(this);
    m_main_window = parent;
    QDialog::hide();
    //隐藏标题栏
    setWindowFlags(Qt::FramelessWindowHint);

    ui->listWidget->setGridSize(QSize(0,20));
//    //设置视频参数-固定码率系数：
//    ui->horizontalSlider_2->setRange(0,50);
//    ui->horizontalSlider_2->setPageStep(15*16);
//    ui->horizontalSlider_2->setTickPosition(ui->horizontalSlider_2->TicksRight);

//    connect(ui->horizontalSlider_2,&QSlider::valueChanged,[=](int val){
//        //int val = ui->horizontalSlider_2->value();
//        ui->lineEdit_slider->setText(QString::number(val));
//    });

    //关闭窗口
    //connect(ui->closeButton,&QPushButton::clicked,this,&QWidget::close);





    //视频参数集成
    m_video_codecs = {
        {"H.264"       , "libx264"  },
        {"VP8"         , "libvpx"   },
        {"Theora"      , "libtheora"},
        {tr("Other..."), "other"    },
    };
    m_audio_codecs = {
        {"Vorbis"          , "libvorbis"   },
        {"MP3"             , "libmp3lame"  },
        {"AAC"             , "libvo_aacenc"},
        {tr("Uncompressed"), "pcm_s16le"   },
        {tr("Other...")    , "other"       },
    };

    // alternative aac codec
    if(!AVCodecIsInstalled(m_audio_codecs[AUDIO_CODEC_AAC].avname)) {
        m_audio_codecs[AUDIO_CODEC_AAC].avname = "aac";
    }

    // load AV container list
    //m_containers_av.clear();

    for(AVOutputFormat *format = av_oformat_next(NULL); format != NULL; format = av_oformat_next(format)) {
        if(format->video_codec == AV_CODEC_ID_NONE)
            continue;
//        ContainerData c;
//        c.name = format->long_name;
//        c.avname = format->name;
//        c.suffixes = SplitSkipEmptyParts(format->extensions, ',');
//        if(c.suffixes.isEmpty()) {
//            c.filter = "";
//        } else {
//            c.filter = tr("%1 files", "This appears in the file dialog, e.g. 'MP4 files'").arg(c.avname) + " (*." + c.suffixes[0];
//            for(int i = 1; i < c.suffixes.size(); ++i) {
//                c.suffixes[i] = c.suffixes[i].trimmed(); // needed because libav/ffmpeg isn't very consistent when they say 'comma-separated'
//                c.filter += " *." + c.suffixes[i];
//            }
//            c.filter += ")";
//        }
        //m_containers_av.push_back(c);
    }
    //std::sort(m_containers_av.begin(), m_containers_av.end());

    // load AV codec list
    m_video_codecs_av.clear();
    m_audio_codecs_av.clear();

    for(AVCodec *codec = av_codec_next(NULL); codec != NULL; codec = av_codec_next(codec)) {

        if(!av_codec_is_encoder(codec))
            continue;
        if(codec->type == AVMEDIA_TYPE_VIDEO && VideoEncoder::AVCodecIsSupported(codec->name)) {
            VideoCodecData c;
            c.name = codec->long_name;
            c.avname = codec->name;
            m_video_codecs_av.push_back(c);
        }
        if(codec->type == AVMEDIA_TYPE_AUDIO && AudioEncoder::AVCodecIsSupported(codec->name)) {
            AudioCodecData c;
            c.name = codec->long_name;
            c.avname = codec->name;
            m_audio_codecs_av.push_back(c);
        }
    }
    std::sort(m_video_codecs_av.begin(), m_video_codecs_av.end());
    std::sort(m_audio_codecs_av.begin(), m_audio_codecs_av.end());

//    if(m_containers_av.empty()) {
//        Logger::LogError("[PageOutput::PageOutput] " + tr("Error: Could not find any suitable container in libavformat!"));
//        throw LibavException();
//    }
    if(m_video_codecs_av.empty()) {
        Logger::LogError("[PageOutput::PageOutput] " + tr("Error: Could not find any suitable video codec in libavcodec!"));
        throw LibavException();
    }
    if(m_audio_codecs_av.empty()) {
        Logger::LogError("[PageOutput::PageOutput] " + tr("Error: Could not find any suitable audio codec in libavcodec!"));
        throw LibavException();
    }



    //connect(ui->comboBox_6, SIGNAL(activated(int)), this, SLOT(OnUpdateVideoCodecFields()));
    //connect(ui->horizontalSlider_2, SIGNAL(valueChanged(int)), ui->lineEdit_slider, SLOT(setNum(int)));

    //音视频参数默认处理
    if(0 == ui->comboBox_6->currentIndex())
    {

        //bit rates
//        ui->label_3->setVisible(false);
//        ui->lineEdit_3->setVisible(false);


    }

    //音视频参数信号槽
        connect(ui->comboBox_7,&QComboBox::currentTextChanged,this,&SettingMenu::SetAudiovisible);
        connect(ui->comboBox_6,&QComboBox::currentTextChanged,this,&SettingMenu::SetVideovisible);

    //快捷键集成
       connect(ui->checkBox_28,SIGNAL(clicked()),this,SLOT(OnUpdateHotkeyFields()));
       connect(ui->checkBox_30, SIGNAL(clicked()), this, SLOT(OnUpdateHotkey()));
       connect(ui->checkBox_29, SIGNAL(clicked()), this, SLOT(OnUpdateHotkey()));
       connect(ui->checkBox_31, SIGNAL(clicked()), this, SLOT(OnUpdateHotkey()));
       connect(ui->checkBox_32, SIGNAL(clicked()), this, SLOT(OnUpdateHotkey()));
       connect(ui->comboBox_16, SIGNAL(activated(int)), this, SLOT(OnUpdateHotkey()));

       //点击取消关闭窗口
       connect(ui->pushButton_10,SIGNAL(clicked()),this,SLOT(close()));
       //点击关闭按钮退出窗口
       //connect(ui->closeButton,SIGNAL(clicked()),this,SLOT(close()));
       //点击确定保存设置退出窗口
       //connect(ui->pushButton_11,SIGNAL(clicked()),this,MainWindow::Quit());//m_main_window

       connect(ui->pushButton_11,&QPushButton::clicked,this,[=](){
               SettingMenu::ConfirmSetting();
       });
       connect(ui->pushButton_11,SIGNAL(clicked()),this,SLOT(close()));
       connect(ui->closeButton,SIGNAL(clicked()),this,SLOT(close()));
       connect(ui->closeButton,&QPushButton::clicked,this,[=](){
               SettingMenu::ConfirmSetting();
       });

       //通用配置信号槽函数
//    m_main_window = new MainPage(this);
       this->installEventFilter(this);
       m_pressed = false;
}


SettingMenu::~SettingMenu()
{
    delete ui;
}

void SettingMenu::SetVideovisible(QString vedio)
{

    QString string_video = ui->comboBox_6->currentText();
    Logger::LogInfo(string_video);
    //H.264
    if(string_video == QString::fromLocal8Bit("H.264"))
    {
        //固定码率系数
//        ui->label_16->setVisible(true);
//        ui->horizontalSlider_2->setVisible(true);

        //编码速

        //bit rates
//        ui->label_3->setVisible(false);
//        ui->lineEdit_3->setVisible(false);



    }
    //VP8
    if(string_video == QString::fromLocal8Bit("VP8"))
    {
        //固定码率系数
//        ui->label_16->setVisible(false);
//        ui->horizontalSlider_2->setVisible(false);
//        ui->lineEdit_slider->setVisible(false);


        //bit rates
//        ui->label_3->setVisible(true);
//        ui->lineEdit_3->setVisible(true);



    }

    //Theora
    if(string_video == QString::fromLocal8Bit("Theora"))
    {
        //固定码率系数
//        ui->label_16->setVisible(false);
//        ui->horizontalSlider_2->setVisible(false);


        //bit rates
//        ui->label_3->setVisible(true);
//        ui->lineEdit_3->setVisible(true);


    }
    //其它
    if(string_video == QString::fromLocal8Bit("其它"))
    {
        //固定码率系数
//        ui->label_16->setVisible(false);
//        ui->horizontalSlider_2->setVisible(false);


        //bit rates
//        ui->label_3->setVisible(true);
//        ui->lineEdit_3->setVisible(true);


        //编码器名

    }

    //变更需求隐藏窗口
//    ui->label_3->setVisible(false);
//    ui->label_16->setVisible(false);
//    ui->lineEdit_3->setVisible(false);
//    ui->horizontalSlider_2->setVisible(false);
//    ui->lineEdit_slider->setVisible(false);

}

void SettingMenu::SetAudiovisible(QString audio)
{

    QString string_voice = ui->comboBox_7->currentText();

    Logger::LogInfo(string_voice);
    if(string_voice == QString::fromLocal8Bit("不压缩"))
    {
        ui->label_20->setVisible(false);
        ui->lineEdit_2->setVisible(false);
        ui->label->setVisible(false);
        ui->label_2->setVisible(false);
        ui->comboBox->setVisible(false);
        ui->lineEdit->setVisible(false);
    }

    if(string_voice != QString::fromLocal8Bit("不压缩") && string_voice != QString::fromLocal8Bit("其它"))
    {
        ui->label_20->setVisible(true);
        ui->lineEdit_2->setVisible(true);
    }

    //音频参数，如果发现是"其它..."则显示1）“编码器名称”；2）自定义选项

    if(string_voice != QString::fromLocal8Bit("其它"))
    {
        ui->label->setVisible(false);
        ui->label_2->setVisible(false);
        ui->comboBox->setVisible(false);
        ui->lineEdit->setVisible(false);
    }
    else
    {
        ui->label->setVisible(true);
        ui->label_2->setVisible(true);
        ui->comboBox->setVisible(true);
        ui->lineEdit->setVisible(true);
    }
}

void SettingMenu::ConfirmSetting()
{
    QSettings settings(CommandLineOptions::GetSettingsFile(), QSettings::IniFormat);
    settings.clear();
    SaveSettings(&settings);

    //设置格式可见性
    //m_main_window = new MainPage;
    int videonumber = GetVideoCodec();
    int audionumber = GetAudioCodec();
    cout<<"videonumber\t"<<videonumber<<endl;
    cout<<"audionumber\t"<<audionumber<<endl;
    m_main_window->SetFormatEnabel(videonumber,audionumber);
}

void SettingMenu::OnUpdateHotkeyFields() {
    bool enabled = IsHotkeyEnabled();
    GroupEnabled({ui->checkBox_30, ui->checkBox_29, ui->checkBox_31, ui->checkBox_32, ui->comboBox_16}, enabled);
    OnUpdateHotkey();
}

void SettingMenu::OnUpdateConfigFields() {
    Logger::LogInfo("OnUpdateConfigFields");
}

void SettingMenu::OnUpdateHotkey() {
    Logger::LogInfo("OnUpdateHotkey");
    if(IsHotkeyEnabled()) {
        unsigned int modifiers = 0;
        if(IsHotkeyCtrlEnabled()) modifiers |= ControlMask;
        if(IsHotkeyShiftEnabled()) modifiers |= ShiftMask;
        if(IsHotkeyAltEnabled()) modifiers |= Mod1Mask;
        if(IsHotkeySuperEnabled()) modifiers |= Mod4Mask;
        int hotkey = GetHotkeyKey();
        cout<<"hotkey\t"<<hotkey<<endl;
        m_hotkey_start_pause.Bind(XK_A + GetHotkeyKey(), modifiers);
    } else {
        m_hotkey_start_pause.Unbind();
    }
}


//读取设置
void SettingMenu::LoadSettings(QSettings* settings) {
    //SetProfile(m_profile_box->FindProfile(settings->value("output/profile", QString()).toString()));//outfile删除此功能
        Logger::LogInfo("==================== " + Logger::tr("LoadSettings") + " ====================");
    //读取快捷键
    SetHotkeyEnabled(settings->value("record/hotkey_enable", true).toBool());
    SetHotkeyCtrlEnabled(settings->value("record/hotkey_ctrl", false).toBool());
    SetHotkeyShiftEnabled(settings->value("record/hotkey_shift", false).toBool());
    SetHotkeyAltEnabled(settings->value("record/hotkey_alt", false).toBool());
    SetHotkeySuperEnabled(settings->value("record/hotkey_super", true).toBool());
    SetHotkeyKey(settings->value("record/hotkey_key", 'r' - 'a').toUInt());

    //读取通用配置
    SetExitWindow(settings->value("record/exit_window",false).toBool());
    SetMinTray(settings->value("record/min_tray",true).toBool());
    SetAtuoHide(settings->value("record/auto_hide",true).toBool());
    SetShowCountdown(settings->value("record/show_countdown",true).toBool());
    OnUpdateHotkeyFields();
    LoadProfileSettings(settings);
}


void SettingMenu::SaveSettings(QSettings* settings) {
    //settings->setValue("output/profile", m_profile_box->GetProfileName());
    //快捷键保存
    settings->setValue("record/hotkey_enable", IsHotkeyEnabled());
    settings->setValue("record/hotkey_ctrl", IsHotkeyCtrlEnabled());
    settings->setValue("record/hotkey_shift", IsHotkeyShiftEnabled());
    settings->setValue("record/hotkey_alt", IsHotkeyAltEnabled());
    settings->setValue("record/hotkey_super", IsHotkeySuperEnabled());
    settings->setValue("record/hotkey_key", GetHotkeyKey());
    //通用配置
    settings->setValue("record/exit_window", IsExitWindow());
    settings->setValue("record/min_tray", IsMinTray());
    settings->setValue("record/auto_hide", IsAtuoHide());
    settings->setValue("record/show_countdown", IsShowCountdown());

    OnUpdateHotkeyFields();
    OnUpdateConfigFields();
    SaveProfileSettings(settings);
}

void SettingMenu::LoadProfileSettingsCallback(QSettings* settings, void* userdata) {
    SettingMenu *page = (SettingMenu*) userdata;
    page->LoadProfileSettings(settings);
}

void SettingMenu::SaveProfileSettingsCallback(QSettings* settings, void* userdata) {
    SettingMenu *page = (SettingMenu*) userdata;
    page->SaveProfileSettings(settings);
}

void SettingMenu::LoadProfileSettings(QSettings* settings) {
    Logger::LogInfo("LoadProfileSettings");

    enum_video_codec default_video_codec = (enum_video_codec) 0;
    for(unsigned int i = 0; i < VIDEO_CODEC_OTHER; ++i) {
        if(AVCodecIsInstalled(m_video_codecs[i].avname)){
            default_video_codec = (enum_video_codec) i;
            break;
        }
    }

    enum_audio_codec default_audio_codec = (enum_audio_codec) 0;
    for(unsigned int i = 0; i < VIDEO_CODEC_OTHER; ++i) {
        if(AVCodecIsInstalled(m_audio_codecs[i].avname)) {
            default_audio_codec = (enum_audio_codec) i;
            break;
        }
    }

    // choose default file name
#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
    QString dir_videos = QStandardPaths::writableLocation(QStandardPaths::MoviesLocation);
    QString dir_documents = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
#else
    QString dir_videos = QDesktopServices::storageLocation(QDesktopServices::MoviesLocation);
    QString dir_documents = QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation);
#endif
    QString dir_home = QDir::homePath();
    QString best_dir = (QDir(dir_videos).exists())? dir_videos : (QDir(dir_documents).exists())? dir_documents : dir_home;

    Logger::LogInfo("SetVideoCodec");
    Logger::LogInfo(settings->value("output/video_codec", QString()).toString());
    SetVideoCodec(StringToEnum(settings->value("output/video_codec", QString()).toString(), default_video_codec));
    //SetVideoKBitRate(settings->value("output/video_kbit_rate", 5000).toUInt());
    //SetH264CRF(settings->value("output/video_h264_crf", 23).toUInt());
    SetVideoAllowFrameSkipping(settings->value("output/video_allow_frame_skipping", true).toBool());

    SetAudioCodec(StringToEnum(settings->value("output/audio_codec", QString()).toString(), default_audio_codec));
    SetAudioCodecAV(FindAudioCodecAV(settings->value("output/audio_codec_av", QString()).toString()));
    SetAudioKBitRate(settings->value("output/audio_kbit_rate", 128).toUInt());
    SetAudioOptions(settings->value("output/audio_options", "").toString());



    // update things
    OnUpdateVideoCodecFields();
    OnUpdateAudioCodecFields();


}

void SettingMenu::OnUpdateVideoCodecFields() {
    enum_video_codec codec = GetVideoCodec();
    MultiGroupVisible({
        //{{ui->label_3, ui->lineEdit_3}, (codec != VIDEO_CODEC_H264)},
        //{{ui->label_16, ui->horizontalSlider_2, ui->lineEdit_slider, ui->label_17, ui->comboBox_5}, (codec == VIDEO_CODEC_H264)},

    });
}

void SettingMenu::OnUpdateAudioCodecFields() {
    enum_audio_codec codec = GetAudioCodec();
    MultiGroupVisible({
        {{ui->label_20, ui->lineEdit_2}, (codec != AUDIO_CODEC_UNCOMPRESSED)},
        {{ui->label, ui->comboBox, ui->label_2, ui->lineEdit}, (codec == AUDIO_CODEC_OTHER)},
    });
}

void SettingMenu::SaveProfileSettings(QSettings* settings) {

    //settings->setValue("output/file", GetFile());
    //settings->setValue("output/separate_files", GetSeparateFiles());
    //settings->setValue("output/add_timestamp", GetAddTimestamp());
    //settings->setValue("output/container", EnumToString(GetContainer()));
    //settings->setValue("output/container_av", m_containers_av[GetContainerAV()].avname);
Logger::LogInfo("save setting");
    //输出界面配置
    settings->setValue("output/video_codec", EnumToString(GetVideoCodec()));
    //settings->setValue("output/video_kbit_rate", GetVideoKBitRate());
    //settings->setValue("output/video_h264_crf", GetH264CRF());
    settings->setValue("output/video_allow_frame_skipping", GetVideoAllowFrameSkipping());

    settings->setValue("output/audio_codec", EnumToString(GetAudioCodec()));
    settings->setValue("output/audio_codec_av", m_audio_codecs_av[GetAudioCodecAV()].avname);
    settings->setValue("output/audio_kbit_rate", GetAudioKBitRate());
    settings->setValue("output/audio_options", GetAudioOptions());
    //record界面配置
    settings->setValue("record/hotkey_enable", IsHotkeyEnabled());
    settings->setValue("record/hotkey_ctrl", IsHotkeyCtrlEnabled());
    settings->setValue("record/hotkey_shift", IsHotkeyShiftEnabled());
    settings->setValue("record/hotkey_alt", IsHotkeyAltEnabled());
    settings->setValue("record/hotkey_super", IsHotkeySuperEnabled());
    settings->setValue("record/hotkey_key", GetHotkeyKey());

//    //input界面
//#if SSR_USE_ALSA
//    settings->setValue("input/audio_alsa_source", GetALSASourceName());
//#endif
//#if SSR_USE_PULSEAUDIO
//    settings->setValue("input/audio_pulseaudio_source", GetPulseAudioSourceName());
//#endif
}

//#if SSR_USE_ALSA
//QString SettingMenu::GetALSASourceName() {
//    return QString::fromStdString(m_alsa_sources[GetALSASource()].m_name);
//}
//#endif

//#if SSR_USE_PULSEAUDIO
//QString SettingMenu::GetPulseAudioSourceName() {
//    return QString::fromStdString(m_pulseaudio_sources[GetPulseAudioSource()].m_name);
//}
//#endif

unsigned int SettingMenu::FindVideoCodecAV(const QString& name) {
    for(unsigned int i = 0; i < m_video_codecs_av.size(); ++i) {
        if(m_video_codecs_av[i].avname == name)
            return i;
    }
    return 0;
}

unsigned int SettingMenu::FindAudioCodecAV(const QString& name) {
    for(unsigned int i = 0; i < m_audio_codecs_av.size(); ++i) {
        if(m_audio_codecs_av[i].avname == name)
            return i;
    }
    return 0;
}

QString SettingMenu::GetVideoCodecAVName() {
    enum_video_codec video_codec = GetVideoCodec();
    if(video_codec != VIDEO_CODEC_OTHER)
        return m_video_codecs[video_codec].avname;
}


QString SettingMenu::GetAudioCodecAVName() {
    enum_audio_codec audio_codec = GetAudioCodec();
    if(audio_codec != AUDIO_CODEC_OTHER)
        return m_audio_codecs[audio_codec].avname;
    else
        return m_audio_codecs_av[GetAudioCodecAV()].avname;
}

int SettingMenu::GetVideoCodecIndex() {
    int videoindex = ui->comboBox_6->currentIndex();
    return videoindex;
}


int SettingMenu::GetAudioCodecIndex() {
    int audioindex = ui->comboBox_7->currentIndex();
    return audioindex;
}

bool SettingMenu::eventFilter(QObject *o, QEvent *e)
{
    if(o == this)
    {
        QMouseEvent *mEvent = (QMouseEvent *)e;
        if(e->type() == QEvent::MouseButtonPress)
        {
            m_pressed = true;
            m_movePos = mEvent->globalPos();
        }
        else if((e->type() == QEvent::MouseMove) && m_pressed)
        {
            QPoint offset = mEvent->globalPos() - m_movePos;
            move(this->x() + offset.x(), this->y() + offset.y());
            m_movePos = mEvent->globalPos();
        }
    }

}

//void SettingMenu::UpdateSysTray() {
//    if(m_systray_icon == NULL)
//        return;
//    GroupEnabled({m_systray_action_cancel, m_systray_action_save}, m_page_started);
//    if(m_page_started) {
//        if(m_error_occurred) {
//            m_systray_icon->setIcon(g_icon_ssr_error);
//        } else if(m_output_started) {
//            m_systray_icon->setIcon(g_icon_ssr_recording);
//        } else {
//            m_systray_icon->setIcon(g_icon_ssr_paused);
//        }
//    } else {
//        m_systray_icon->setIcon(g_icon_ssr_idle);
//    }
//    if(m_page_started && m_output_started) {
//        m_systray_action_start_pause->setIcon(g_icon_pause);
//        m_systray_action_start_pause->setText(tr("Pause recording"));
//    } else {
//        m_systray_action_start_pause->setIcon(g_icon_record);
//        m_systray_action_start_pause->setText(tr("Start recording"));
//    }
//}

static std::vector<std::pair<QString, QString> > GetOptionsFromString(const QString& str) {
    std::vector<std::pair<QString, QString> > options;
    QStringList optionlist = SplitSkipEmptyParts(str, ',');
    for(int i = 0; i < optionlist.size(); ++i) {
        QString a = optionlist[i];
        int p = a.indexOf('=');
        if(p < 0) {
            options.push_back(std::make_pair(a.trimmed(), QString()));
        } else {
            options.push_back(std::make_pair(a.mid(0, p).trimmed(), a.mid(p + 1).trimmed()));
        }
    }
    return options;
}

//提供 HAN
void SettingMenu::StartPage() {

    // save the settings in case libav/ffmpeg decides to kill the process
//    m_main_window = new MainPage();
    m_main_window->SaveSettings();

    // clear the log
    textEdit->clear();


    // get the audio input settings
    //m_audio_enabled = GetAudioEnabled();
//    m_audio_channels = 2;
//    m_audio_sample_rate = 48000;
//#if SSR_USE_ALSA
//    m_alsa_source = GetALSASourceName();
//#endif
//#if SSR_USE_PULSEAUDIO
//    m_pulseaudio_source = GetPulseAudioSourceName();
//#endif

    // override sample rate for problematic cases (these are hard-coded for now)
    if(ui->comboBox_6->currentIndex() == CONTAINER_OTHER) {
        m_audio_sample_rate = 44100;
    }

    // get the output settings
    m_output_settings.video_codec_avname = GetVideoCodecAVName();
    //m_output_settings.video_kbit_rate = GetVideoKBitRate();
    m_output_settings.video_options.clear();

    m_output_settings.video_frame_rate = m_video_frame_rate;


    m_output_settings.audio_codec_avname = (m_audio_enabled)? GetAudioCodecAVName() : QString();
    m_output_settings.audio_kbit_rate = GetAudioKBitRate();
    m_output_settings.audio_options.clear();
    m_output_settings.audio_channels = m_audio_channels;
    m_output_settings.audio_sample_rate = m_audio_sample_rate;

//    // some codec-specific things
//    // you can get more information about all these options by running 'ffmpeg -h' or 'avconv -h' from a terminal
    switch(GetVideoCodec()) {
        case VIDEO_CODEC_H264: {
            // x264 has a 'constant quality' mode, where the bit rate is simply set to whatever is needed to keep a certain quality. The quality is set
            // with the 'crf' option. 'preset' changes the encoding speed (and hence the efficiency of the compression) but doesn't really influence the quality,
            // which is great because it means you don't have to experiment with different bit rates and different speeds to get good results.
            //m_output_settings.video_options.push_back(std::make_pair(QString("crf"), QString::number(GetH264CRF())));
            break;
        }
//        case VIDEO_CODEC_VP8: {
//            // The names of there parameters are very unintuitive. The two options we care about (because they change the speed) are 'deadline' and 'cpu-used'.
//            // 'deadline=best' is unusably slow. 'deadline=good' is the normal setting, it tells the encoder to use the speed set with 'cpu-used'. Higher
//            // numbers will use *less* CPU, confusingly, so a higher number is faster. I haven't done much testing with 'realtime' so I'm not sure if it's a good idea here.
//            // It sounds useful, but I think it will use so much CPU that it will slow down the program that is being recorded.
//            m_output_settings.video_options.push_back(std::make_pair(QString("deadline"), QString("good")));
//            break;
//        }
        case VIDEO_CODEC_OTHER: {
            break;
        }
        default: break; // to keep GCC happy
    }
    switch(GetAudioCodec()) {
        case AUDIO_CODEC_OTHER: {
            m_output_settings.audio_options = GetOptionsFromString(GetAudioOptions());
            break;
        }
        default: break; // to keep GCC happy
    }


}

//读取通用配置
bool SettingMenu::is_exit_windows()
{
    bool exitwindow = ui->radioButton_4->isChecked();
    cout<<"window\t"<<exitwindow<<endl;
    return exitwindow;
}

bool SettingMenu::is_min_tray()
{
    bool mintray = ui->radioButton_3->isChecked();
    cout<<"mintray\t"<<mintray<<endl;
    return mintray;
}

bool SettingMenu::hide()
{
    bool hide = ui->checkBox_10->isChecked();
    cout<<"hide\t"<<hide<<endl;
    return hide;
}


#ifndef SETTINGMENU_H
#define SETTINGMENU_H


#include "Global.h"
//#include <QWidget>
#include "HotkeyListener.h"
#include "ui_SettingMenu.h"
#include "LogInfo.h"

#if SSR_USE_ALSA
#include "ALSAInput.h"
#endif

#if SSR_USE_PULSEAUDIO
#include "PulseAudioInput.h"
#endif
#include "OutputManager.h"
#include "OutputSettings.h"
#include <QDialog>

class MainPage;

namespace Ui {
class SettingMenu;
}

class SettingMenu : public QDialog
{
    Q_OBJECT

public:
    explicit SettingMenu(MainPage *parent = 0);
    ~SettingMenu();

    Ui::SettingMenu *ui;
    enum enum_container {
        CONTAINER_MKV,
        CONTAINER_MP4,
        CONTAINER_WEBM,
        CONTAINER_OGG,
        CONTAINER_OTHER,
        CONTAINER_COUNT // must be last
    };
    enum enum_video_codec {
        VIDEO_CODEC_H264,
//        VIDEO_CODEC_VP8,
        VIDEO_CODEC_OTHER,
        VIDEO_CODEC_THEORA,
        VIDEO_CODEC_COUNT // must be last
    };
    enum enum_audio_codec {
        AUDIO_CODEC_VORBIS,
        AUDIO_CODEC_MP3,
        AUDIO_CODEC_AAC,
        AUDIO_CODEC_UNCOMPRESSED,
        AUDIO_CODEC_OTHER,
        AUDIO_CODEC_COUNT // must be last
    };
    enum enum_h264_preset {
        H264_PRESET_ULTRAFAST,
        H264_PRESET_SUPERFAST,
        H264_PRESET_VERYFAST,
        H264_PRESET_FASTER,
        H264_PRESET_FAST,
        H264_PRESET_MEDIUM,
        H264_PRESET_SLOW,
        H264_PRESET_SLOWER,
        H264_PRESET_VERYSLOW,
        H264_PRESET_PLACEBO,
        H264_PRESET_COUNT // must be last
    };
    //inline enum_schedule_time_zone GetScheduleTimeZone() { return m_schedule_time_zone; }
    //inline std::vector<ScheduleEntry> GetScheduleEntries() { return m_schedule_entries; }
    inline bool IsHotkeyEnabled() { return ui->checkBox_28->isChecked(); }
    inline bool IsHotkeyCtrlEnabled() { return ui->checkBox_30->isChecked();}
    inline bool IsHotkeyShiftEnabled() { return ui->checkBox_29->isChecked(); }
    inline bool IsHotkeyAltEnabled() { return ui->checkBox_31->isChecked(); }
    inline bool IsHotkeySuperEnabled() { return ui->checkBox_32->isChecked(); }
    inline unsigned int GetHotkeyKey() { return ui->comboBox_16->currentIndex(); }
    //inline void SetScheduleTimeZone(enum_schedule_time_zone time_zone) { m_schedule_time_zone = (enum_schedule_time_zone) clamp((unsigned int) time_zone, 0u, (unsigned int) SCHEDULE_TIME_ZONE_COUNT - 1); }
    //inline void SetScheduleEntries(const std::vector<ScheduleEntry>& schedule) { m_schedule_entries = schedule; }
    inline void SetHotkeyEnabled(bool enable) { ui->checkBox_28->setChecked(enable); }
    inline void SetHotkeyCtrlEnabled(bool enable) { ui->checkBox_30->setChecked(enable); }
    inline void SetHotkeyShiftEnabled(bool enable) { ui->checkBox_29->setChecked(enable); }
    inline void SetHotkeyAltEnabled(bool enable) { ui->checkBox_31->setChecked(enable); }
    inline void SetHotkeySuperEnabled(bool enable) { ui->checkBox_32->setChecked(enable); }
    inline void SetHotkeyKey(unsigned int key) { ui->comboBox_16->setCurrentIndex(clamp(key, 0u, 25u)); }

    //inline unsigned int GetProfile() { return m_profile_box->GetProfile(); }
    //inline QString GetFile() { return m_lineedit_file->text(); }
    //inline bool GetSeparateFiles() { return m_checkbox_separate_files->isChecked(); }
    //inline bool GetAddTimestamp() { return m_checkbox_add_timestamp->isChecked(); }
    //inline enum_container GetContainer() { return (enum_container) clamp(m_combobox_container->currentIndex(), 0, CONTAINER_COUNT - 1); }
    //inline unsigned int GetContainerAV() { return clamp(m_combobox_container_av->currentIndex(), 0, (int) m_containers_av.size() - 1); }
    inline enum_video_codec GetVideoCodec() { return (enum_video_codec) clamp(ui->comboBox_6->currentIndex(), 0, VIDEO_CODEC_COUNT - 1); }
    //inline unsigned int GetVideoKBitRate() { return ui->lineEdit_3->text().toUInt(); }
    //inline unsigned int GetH264CRF() { return ui->horizontalSlider_2->value(); }

    inline bool GetVideoAllowFrameSkipping() { return ui->checkBox_11->isChecked(); }

    //音频
    inline enum_audio_codec GetAudioCodec() { return (enum_audio_codec) clamp(ui->comboBox_7->currentIndex(), 0, AUDIO_CODEC_COUNT - 1); }
    inline unsigned int GetAudioCodecAV() { return clamp(ui->comboBox->currentIndex(), 0, (int) m_audio_codecs_av.size() - 1); }
    inline unsigned int GetAudioKBitRate() { return ui->lineEdit_2->text().toUInt(); }
    inline QString GetAudioOptions() { return ui->lineEdit->text(); }

    //通用配置
    inline bool IsExitWindow() { return ui->radioButton_4->isChecked(); }
    inline bool IsMinTray() { return ui->radioButton_3->isChecked(); }
    inline bool IsAtuoHide() { return ui->checkBox_10->isChecked(); }
    inline bool IsShowCountdown() { return ui->checkBox_9->isChecked(); }

    inline void SetExitWindow(bool enable) { ui->radioButton_4->setChecked(enable); }
    inline void SetMinTray(bool enable) { ui->radioButton_3->setChecked(enable); }
    inline void SetAtuoHide(bool enable) { ui->checkBox_10->setChecked(enable); }
    inline void SetShowCountdown(bool enable) { ui->checkBox_9->setChecked(enable); }
    //inline bool GetAudioEnabled() { return m_checkbox_audio_enable->isChecked(); }

public:
    void LoadSettings(QSettings* settings);
    void SaveSettings(QSettings* settings);
    //bool ShouldBlockClose();
#if SSR_USE_ALSA
    QString GetALSASourceName();
#endif

#if SSR_USE_PULSEAUDIO
    QString GetPulseAudioSourceName();
#endif

//#if SSR_USE_ALSA
//    inline unsigned int GetALSASource() { return clamp(m_combobox_alsa_source->currentIndex(), 0, (int) m_alsa_sources.size() - 1); }
//#endif
//#if SSR_USE_PULSEAUDIO
//    inline unsigned int GetPulseAudioSource() { return clamp(m_combobox_pulseaudio_source->currentIndex(), 0, (int) m_pulseaudio_sources.size() - 1); }
//#endif
    void SetVideovisible(QString vedio);
    void SetAudiovisible(QString vedio);
    void StartPage();
    QString GetVideoCodecAVName();
    QString GetAudioCodecAVName();
    bool is_exit_windows();
    bool is_min_tray();
    bool hide();
    void close_setting_windows();
    HotkeyCallback m_hotkey_start_pause;
    int GetVideoCodecIndex();
    int GetAudioCodecIndex();

protected:
    bool eventFilter(QObject *o, QEvent *e);
private:
    QPoint m_movePos;
    bool m_pressed;

private:
    MainPage *m_main_window;
    static void LoadProfileSettingsCallback(QSettings* settings, void* userdata);
    static void SaveProfileSettingsCallback(QSettings* settings, void* userdata);
    void LoadProfileSettings(QSettings* settings);
    void SaveProfileSettings(QSettings* settings);
    void ConfirmSetting();
//    struct ContainerData {
//        QString name, avname;
//        QStringList suffixes;
//        QString filter;
//        std::set<enum_video_codec> supported_video_codecs;
//        std::set<enum_audio_codec> supported_audio_codecs;
//        inline bool operator<(const ContainerData& other) const { return (avname < other.avname); }
//    };
    struct VideoCodecData {
        QString name, avname;
        inline bool operator<(const VideoCodecData& other) const { return (avname < other.avname); }
    };
    struct AudioCodecData {
        QString name, avname;
        inline bool operator<(const AudioCodecData& other) const { return (avname < other.avname); }
    };
#if SSR_USE_ALSA
    std::vector<ALSAInput::Source> m_alsa_sources;
#endif
#if SSR_USE_PULSEAUDIO
    bool m_pulseaudio_available;
    std::vector<PulseAudioInput::Source> m_pulseaudio_sources;
#endif
private:
    //Ui::setting_menu_new *ui;


    //std::vector<ContainerData> m_containers, m_containers_av;
    std::vector<VideoCodecData> m_video_codecs, m_video_codecs_av;
    std::vector<AudioCodecData> m_audio_codecs, m_audio_codecs_av;
//    std::vector<VideoCodecData> m_video_codecs;
//    std::vector<AudioCodecData> m_audio_codecs;
    QTextEdit *textEdit;
    bool m_audio_enabled;
    unsigned int m_audio_channels,m_audio_sample_rate;
#if SSR_USE_ALSA
    QString m_alsa_source;
#endif
#if SSR_USE_PULSEAUDIO
    QString m_pulseaudio_source;
#endif

    OutputSettings m_output_settings;
    std::unique_ptr<OutputSettings> m_output_manager;
    unsigned int m_video_frame_rate;
    void UpdateSysTray();
    QSystemTrayIcon *m_systray_icon;
    QAction *m_systray_action_start_pause, *m_systray_action_cancel, *m_systray_action_save;
    QAction *m_systray_action_show_hide, *m_systray_action_quit;
public slots:
    void OnUpdateHotkeyFields();
    void OnUpdateHotkey();
    void OnUpdateVideoCodecFields();
    void OnUpdateAudioCodecFields();
    void OnUpdateConfigFields();




public:
    //inline void SetProfile(unsigned int profile) { m_profile_box->SetProfile(profile); }
    //inline void SetFile(const QString& file) { m_lineedit_file->setText(file); }
    //inline void SetSeparateFiles(bool separate_files) { m_checkbox_separate_files->setChecked(separate_files); }
    //inline void SetAddTimestamp(bool add_timestamp) { m_checkbox_add_timestamp->setChecked(add_timestamp); }
    inline void SetVideoCodec(enum_video_codec video_codec) { ui->comboBox_6->setCurrentIndex(clamp((unsigned int) video_codec, 0u, (unsigned int) VIDEO_CODEC_COUNT - 1)); }
    //inline void SetVideoKBitRate(unsigned int kbit_rate) { ui->lineEdit_3->setText(QString::number(kbit_rate)); }
    //inline void SetH264CRF(unsigned int crf) { ui->horizontalSlider_2->setValue(crf); }

    inline void SetVideoAllowFrameSkipping(bool allow_frame_skipping) { return ui->checkBox_11->setChecked(allow_frame_skipping); }

    //音频
    inline void SetAudioCodec(enum_audio_codec audio_codec) { ui->comboBox_7->setCurrentIndex(clamp((unsigned int) audio_codec, 0u, (unsigned int) AUDIO_CODEC_COUNT - 1)); }
    inline void SetAudioCodecAV(unsigned int audio_codec_av) { ui->comboBox->setCurrentIndex(clamp(audio_codec_av, 0u, (unsigned int) m_audio_codecs_av.size() - 1)); }
    inline void SetAudioKBitRate(unsigned int kbit_rate) { ui->lineEdit_2->setText(QString::number(kbit_rate)); }
    inline void SetAudioOptions(const QString& options) { ui->lineEdit->setText(options); }

private:
    unsigned int FindContainerAV(const QString& name);
    unsigned int FindVideoCodecAV(const QString& name);
    unsigned int FindAudioCodecAV(const QString& name);
};

#endif // SETTINGMENU_H

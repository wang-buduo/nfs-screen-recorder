#include <QString>
#include <QtTest>
#include "CPUFeatures.h"
#include "Logger.h"
#include "QueueBuffer.h"
#include "Global.h"
#include <iostream>
#include "AVWrapper.h"
#include "CommandLineOptions.h"

class SsrTstTest : public QObject
{
    Q_OBJECT

public:
    SsrTstTest();

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();
    void testLogInfo();
    void testLogInit();
    void testQueueBufferInitial();
    void testLogError();
    void testQueueBufferIsEmpty();
    void testQueueBufferGetData();
    void testGLInjectException();
    void testSSRStreamException();
    void testX11Exception();
    void testResamplerException();
    void testLibavException();
    void testIsPlatformX11();
    void test_hrt_time_micro();
    void test_GetUserName();
    void test_grow_align16();
    void testAVFormatIsInstalled();
    void testAVCodecIsInstalled();
    void testAVCodecSupportsSampleFormat();
    void testAVCodecSupportsPixelFormat();
    void testHasAVX();
    void testHasAVX2();
    void testHasMMX();
    void testHasSSE();
    void testHasSSE2();
    void testHasSSE3();
    void testHasSSSE3();
    void testHasSSE41();\
    void testHasSSE42();
    void testHasBMI1();
    void testHasBMI2();
    void testGetSettingsFile();
    void testV4L2Exception();
    void testALSAException();
    void testPulseAudioException();
    void testJACKException();
    void testGetLogFile();
    void testGetStatsFile();
    void testGetRedirectStderr();
    void testGetSysTray();
    void testGetStartHidden();
    void testGetStartRecording();
    void testGetActivateSchedule();
    void testGetSyncDiagram();
    void testGetBenchmark();
    void testGetGui();
};

SsrTstTest::SsrTstTest()
{

}

void SsrTstTest::initTestCase()
{
}
CommandLineOptions * ins = new CommandLineOptions();
CommandLineOptions * co = CommandLineOptions::GetInstance();
void SsrTstTest::testGetRedirectStderr()
{
    bool ret = co->GetRedirectStderr();
    QVERIFY(ret);
}

void SsrTstTest::testGetSysTray()
{
    bool ret = co->GetSysTray();
    QVERIFY(ret);
}

void SsrTstTest::testGetStartHidden()
{
    bool ret = co->GetStartHidden();
    QVERIFY(!ret);
}

void SsrTstTest::testGetStartRecording()
{
    bool ret = co->GetStartRecording();
    QVERIFY(!ret);
}

void SsrTstTest::testGetActivateSchedule()
{
    bool ret = co->GetActivateSchedule();
    QVERIFY(!ret);
}

void SsrTstTest::testGetSyncDiagram()
{
    bool ret = co->GetSyncDiagram();
    QVERIFY(!ret);
}

void SsrTstTest::testGetBenchmark()
{
    bool ret = co->GetBenchmark();
    QVERIFY(!ret);
}
void SsrTstTest::testGetGui()
{
    bool ret = co->GetGui();
    QVERIFY(ret);
}

void SsrTstTest::testGetSettingsFile()
{

    QString ret = co->GetSettingsFile();
    QVERIFY(ret != NULL);

}



void SsrTstTest::testV4L2Exception(){
    QVERIFY(1);
}

void SsrTstTest::testALSAException()
{
    QVERIFY(1);

}

void SsrTstTest::testPulseAudioException()
{
    QVERIFY(1);
}

void SsrTstTest::testJACKException()
{
    QVERIFY(1);
}


void SsrTstTest::testGetLogFile()
{

    QString ret = co->GetLogFile();
    QVERIFY(ret == NULL);
}

void SsrTstTest::testGetStatsFile()
{

    QString ret = co->GetStatsFile();
    QVERIFY(ret == NULL);
}

void SsrTstTest::testAVCodecIsInstalled()
{
    bool ret = AVCodecIsInstalled("libx264");
    QVERIFY(ret);
}

void SsrTstTest::testAVCodecSupportsPixelFormat()
{
    AVCodec * ac = new AVCodec();
    bool ret = AVCodecSupportsPixelFormat(ac, AV_PIX_FMT_BGR24);
    QVERIFY(ret);
}

void SsrTstTest::testAVCodecSupportsSampleFormat()
{
    AVCodec * ac = new AVCodec();
    bool ret = AVCodecSupportsSampleFormat(ac, AV_SAMPLE_FMT_S32);
    QVERIFY(ret);
}

void SsrTstTest::testHasAVX()
{
    CPUFeatures * cf = new CPUFeatures();

    QVERIFY(!cf->HasAVX());
}

void SsrTstTest::testHasAVX2()
{
    CPUFeatures * cf = new CPUFeatures();
    QVERIFY(!cf->HasAVX2());
}

void SsrTstTest::testHasMMX()
{
    CPUFeatures * cf = new CPUFeatures();
    QVERIFY(!cf->HasMMX());
}

void SsrTstTest::testHasSSE()
{
    CPUFeatures * cf = new CPUFeatures();
    QVERIFY(!cf->HasSSE());
}

void SsrTstTest::testHasSSE2()
{
    CPUFeatures * cf = new CPUFeatures();
    QVERIFY(!cf->HasSSE2());
}

void SsrTstTest::testHasSSE3()
{
    CPUFeatures * cf = new CPUFeatures();
    QVERIFY(!cf->HasSSE3());
}

void SsrTstTest::testHasSSSE3()
{
    CPUFeatures * cf = new CPUFeatures();
    QVERIFY(!cf->HasSSSE3());
}

void SsrTstTest::testHasSSE41()
{
    CPUFeatures * cf = new CPUFeatures();
    QVERIFY(!cf->HasSSE41());
}

void SsrTstTest::testHasSSE42()
{
    CPUFeatures * cf = new CPUFeatures();
    QVERIFY(!cf->HasSSE42());
}

void SsrTstTest::testHasBMI1()
{
    CPUFeatures * cf = new CPUFeatures();
    QVERIFY(!cf->HasBMI1());
}

void SsrTstTest::testHasBMI2()
{
    CPUFeatures * cf = new CPUFeatures();
    QVERIFY(!cf->HasBMI2());
}

void SsrTstTest::testLogInfo()
{
    Logger * lg = new Logger();
    lg->LogInfo("testLogInfo");
    QVERIFY2(true, "Failure");
}

void SsrTstTest::testLogInit()
{
    Logger * lg_ins = Logger::GetInstance();
    int m_stderr = lg_ins->m_original_stderr;
    QVERIFY(m_stderr == -1);
}

void SsrTstTest::testQueueBufferInitial()
{
    QueueBuffer<int> * qb = new QueueBuffer<int>;
    QVERIFY(qb->GetData());
}

void SsrTstTest::test_grow_align16()
{
    QVERIFY(grow_align16(0) == 0);
}

void SsrTstTest::testLogError()
{
    Logger * lg_ins = Logger::GetInstance();
    lg_ins->LogError("test log error");
    QVERIFY(true);
}

void SsrTstTest::testQueueBufferIsEmpty()
{
    QueueBuffer<int> * qb = new QueueBuffer<int>;
    QVERIFY(qb->IsEmpty());
}

void SsrTstTest::testQueueBufferGetData()
{
    QueueBuffer<int> * qb = new QueueBuffer<int>;
    qb->GetData();
    QVERIFY(qb->GetData() != NULL);
}

void SsrTstTest::testGLInjectException()
{
    GLInjectException * gj = new GLInjectException();
    QVERIFY(gj->what() == "GLInjectException");
}


void SsrTstTest::testSSRStreamException()
{
    SSRStreamException * ss = new SSRStreamException();

    QVERIFY(ss->what() == "SSRStreamException");
}

void SsrTstTest::testX11Exception()
{
    X11Exception * xe = new X11Exception();
    QVERIFY(xe->what() == "X11Exception");
}

void SsrTstTest::testResamplerException()
{
    ResamplerException * re = new ResamplerException();
    QVERIFY(re->what() == "ResamplerException");
}

void SsrTstTest::testLibavException()
{
    LibavException * le = new LibavException();
    QVERIFY(le->what() == "LibavException");
}


void SsrTstTest::testIsPlatformX11()
{
    QVERIFY(!IsPlatformX11());

}


void SsrTstTest::test_hrt_time_micro()
{
    QVERIFY(hrt_time_micro() != 0);
}

void SsrTstTest::test_GetUserName()
{
    GetUserName();
    QVERIFY(1);
}
void SsrTstTest::testAVFormatIsInstalled()
{
    bool ret = AVFormatIsInstalled("mp4");
    QVERIFY(ret);
}

void SsrTstTest::cleanupTestCase()
{
}
QTEST_APPLESS_MAIN(SsrTstTest)

#include "tst_ssrtsttest.moc"

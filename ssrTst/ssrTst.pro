#-------------------------------------------------
#
# Project created by QtCreator 2022-07-18T16:04:30
#
#-------------------------------------------------

QT       += testlib x11extras gui widgets


TARGET = tst_ssrtsttest
CONFIG   += console
CONFIG   -= app_bundle
LIBS += -lavformat -lavcodec -lavutil -lswscale -lX11 -lXext -lXfixes -lasound -ljack -lpulse -lXi -lXinerama -lXcursor -lXrandr

TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += tst_ssrtsttest.cpp \
    CPUFeatures.cpp \
    Logger.cpp \
    Benchmark.cpp \
    AVWrapper.cpp \
    CommandLineOptions.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += \
    CPUFeatures.h \
    Global.h \
    Logger.h \
    QueueBuffer.h \
    Benchmark.h \
    AVWrapper.h \
    FastScaler_Convert.h \
    FastScaler_Scale.h \
    CommandLineOptions.h

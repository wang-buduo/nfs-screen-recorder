项目介绍
----

方德录屏基于SimpleScreenRecorder开源，支持多种视频格式输出，快速录屏、上手简单。完成在各种场景下的录屏操作，可以保存为mp4,mkv,webm,ogg多种视频格式。

## 安装

1、安装依赖

sudo apt install 

2、打包

cd  nfs-screen-recorder

dpkg-buildpackage -uc -us

3、安装包

sudo dpkg -i nfs-screen-recorder_1.0.0-1_amd64.deb

## 帮助



## 开源许可证

nfs-screen-recorder 在GPLv2下发布，请参考项目中COPYING文件查看详情。


